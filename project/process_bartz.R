library(statnet)



network.types <- c("ETK", "ET", "ETK3")

if (Sys.getenv("SLURM_ARRAY_TASK_ID") != "") {
	my_id <- as.numeric(Sys.getenv("SLURM_ARRAY_TASK_ID"))
} else {
	my_id <- 3
	}

ntype.id <- ((my_id-1)%%length(network.types)) +1
ntype <- network.types[[ntype.id]]

Rdata_files <- getting.ntype.files(ntype, experiment="")
merged.comparisons <- merge.comparisons(Rdata_files)

ivar = 1
edges.theta <- sapply(merged.comparisons["mcmcsgd.fit",], FUN=extract.theta_record, ivar=ivar)
edges.mcmle <- sapply(merged.comparisons["ergm.fit",], FUN=extract.ergm_record, ivar=ivar)
edges.truth <- sapply(merged.comparisons["truth",], FUN=extract.truth, ivar=ivar)
edges.init  <- sapply(merged.comparisons["ergm.fit",], FUN=extract.init, ivar=ivar)
processed <- list(
	mcsgd=edges.theta,
	mcmle=edges.mcmle,
	truth=edges.truth,
	init=edges.init,
	)

save(processed, file=sprintf("output/processed_%s.Rdata", ntype))

wrongness <- function(estimate, truth){
	return(sqrt(sum((estimate-truth)^2)))
}

median.wrongness <- function(estimate, truth){
	return(median(abs(estimate-truth)))
}

for (iiter in 1:5){
	#mcmle.ith <- sapply(edges.mcmle, function(l)l[[iiter]])
	mcmle.ith <- processed$mcmle[iiter,]
	mcsgd.ith <- procsedded$mcsgd[iiter,]
	init <- processed$init
	truth <- processed$truth
	
	
	init.wrong <- median.wrongness(init, truth)
	mcsgd.wrong <- median.wrongness(mcsgd.ith, truth)
	mcmle.wrong <- median.wrongness(mcmle.ith, truth)
	print("INIT WRONG")
	print(init.wrong)
	print("MC MLE wrong")
	print(mcmle.wrong)
	print("MC SGD wrong")
	print(mcsgd.wrong)
}
