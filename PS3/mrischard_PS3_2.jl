module Question2

using Base.LinAlg: qrfact
using PyPlot
import Optim
import Distributions
#=using StatsBase=#

function random_orthogonal(p::Int64)
	B = rand(p,p)
	A = full(qrfact(B)[:Q])
	return A
end

immutable SampleData
	X::Array{Float64,2}
	Y::Array{Float64,2}
	A::Array{Float64,2}
	theta::Array{Float64,1}
	n::Int64
	p::Int64
	Q::Array{Float64,2}
	lambdas::Array{Float64,1}
end

type SGDMethod
	sgd::Function
	name::String
	c::Float64
	opt_a::Float64
	opt_gamma::Float64
end

immutable LearningParameters
	gamma0::Float64
	a::Float64
	c::Float64
end

function learning_rate(params::LearningParameters, t::Int64)
	return params.gamma0 * (1 + params.a*params.gamma0*t)^(-params.c)
end

function generic_learning_rate(gamma0::Float64, a::Float64, c::Float64, t::Int64)
	return gamma0 * (1 + a*gamma0*t)^(-c)
end


function gen_lambdas_2a(dim_p)
	lambdas = ones(dim_p)*0.02
	lambdas[1:3] = 1.0
	return lambdas
end

function sample_data_2a(dim_n::Int64, dim_p::Int64)
	Q = random_orthogonal(dim_p)
	lambdas = gen_lambdas_2a(dim_p)
	A = Q * Diagonal(lambdas) * transpose(Q)
	mvn = Distributions.MvNormal(eye(dim_p))
	X = transpose(rand(mvn, dim_n)) # each row a random samples from mvn
	#=theta = ones(Float64, dim_p, 1)=#
	#=epsilon = randn(dim_n)=#
	#=y = X * theta + epsilon=#
	#=return Dict{String,Any}(Y=>y, X=>X, A=>A, theta=>theta)=#
	theta = zeros(dim_p)
	return SampleData(X, X, A, vec(theta), dim_n, dim_p, Q, lambdas)
end

function gen_lambdas_2b(dim_p)
	linspace(0.01, 1, dim_p)
end

function sample_data_2b(dim_n::Int64, dim_p::Int64)
	lambdas,A,Q = random_covar(dim_p)
	return sample_data_2b_from_A(dim_n, dim_p, A, Q, lambdas)
end

function random_covar(dim_p::Int64)
	Q = random_orthogonal(dim_p)
	lambdas = gen_lambdas_2b(dim_p)
	A = Q * Diagonal(lambdas) * transpose(Q)
	return (lambdas,A,Q)
end

function sample_data_2b_from_A(dim_n::Int64, dim_p::Int64, 
	A::Array{Float64,2}, Q::Array{Float64,2}, lambdas::Array{Float64,1})
	mvn = Distributions.MvNormal(A)
	X = transpose(rand(mvn, dim_n)) # each row a random samples from mvn
	theta = ones(Float64, dim_p, 1)
	epsilon = randn(dim_n)
	y = X * theta + epsilon
	return SampleData(X, y, A, vec(theta), dim_n, dim_p, Q, lambdas)
end

function sgd_2a(data::SampleData, lpars::LearningParameters)
	n = data.n
	p = data.p
	theta_old = randn(p)
	theta_sgd::Array{Float64,2} = ones(Float64, p, n+1)
	theta_sgd[:,1] = theta_old
	A = data.A
	Y = transpose(data.Y)
	for t = 1:n
		#=learning rate=#
		x = Y[:,t] # px1
		ai::Float64 = learning_rate(lpars, t)
		grad = 2 * transpose(theta_old - x) * A
		theta_new = theta_old - ai*transpose(grad)
		theta_old = theta_new
		theta_sgd[:,t+1] = theta_new
	end
	return theta_sgd
end

function implicit_2a(data::SampleData, lpars::LearningParameters)
	n = data.n
	p = data.p
	theta_old = randn(p)
	theta_imp::Array{Float64,2} = ones(Float64, p, n+1)
	theta_imp[:,1] = theta_old
	A = data.A
	Y = transpose(data.Y)
	Q = data.Q
	Qt = transpose(Q)
	for t = 1:n
		#=learning rate=#
		x = Y[:,t] # px1
		ai::Float64 = learning_rate(lpars, t)
		rhs::Array{Float64,1} = theta_old + 2*ai*A*x
		theta_coef_eigvals::Array{Float64,1} = 2*ai*data.lambdas+1
		inv_eigvals::Array{Float64,1} = 1 ./ theta_coef_eigvals
		inv_matrix::Array{Float64,2} = Q * Diagonal(inv_eigvals) * Qt
		theta_new = vec(inv_matrix*rhs)
		theta_old = theta_new
		theta_imp[:,t+1] = theta_new
	end
	return theta_imp
end

function sgd_2b(X::Array{Float64,2}, Y::Array{Float64,2}, N::Int64, p::Int64, lpars::LearningParameters)
	theta_sgd::Array{Float64,2} = Array(Float64, p, N)
	theta_latest = theta_sgd[:,1]
	Xt = transpose(X)
	@assert size(X) == (N,p)
	@assert size(Y) == (N,1)
	for t = 1:N
		@inbounds xi = Xt[:,t] # px1
		@inbounds yi = Y[t]
		ai::Float64 = learning_rate(lpars, t)
		lpred = dot(theta_latest, xi)
		#=theta_new = (theta_old - ai * lpred * xi) + ai * yi * xi=#
		theta
		for j=1:p
			@inbounds theta_latest[j] += ai * (yi-lpred)*xi[j]
		end
		@inbounds theta_sgd[:,t] = theta_latest
	end
	return theta_sgd
end

function asgd_2b(data::SampleData, lpars::LearningParameters)
	theta_sgd = sgd_2b(data, lpars)
	return asgd(theta_sgd)
end

function sgd_2b(data::SampleData, lpars::LearningParameters)
	N = data.n
	p = data.p
	X = data.X
	Y = data.Y
	return sgd_2b(X, Y, N, p, lpars)
end

function batch(data::SampleData)
	n, p = size(data.Y)
	theta_batch = zeros(Float64, p, n+1)
	running_average = cumsum(data.Y, 1) ./ reshape([1:n], n, 1)
	theta_batch[:,2:end] = transpose(running_average)
	return theta_batch
end

function get_cost(A::Array{Float64,2}, theta_sgd::Array{Float64,2}, theta_true::Array{Float64,1})
	_, n = size(theta_sgd)
	cost = zeros(Float64, n)
	for t = 1:n
		theta_t = theta_sgd[:,t]
		residual = theta_t - theta_true
		square = transpose(residual) * A * residual
		cost[t] = square[1]
	end
	return cost
end

function asgd(theta_sgd)
	p, n = size(theta_sgd)
	avg_theta = cumsum(theta_sgd, 2) ./ reshape([1:n], 1, n)
	return avg_theta
end

type Plottable
	x::Array{Float64,1}
	cost::Array{Float64,1}
	color::String
	linestyle::String
	label::String
end

function logSamplePlottable(data::SampleData, theta::Array{Float64,2}, color="red", linestyle="-", label="LabelMe"; n=100)
	logsample = int(floor(logspace(1,log10(data.n),n)))
	cost = get_cost(data.A, theta[:,logsample], data.theta)
	return Plottable(logsample, cost, color, linestyle, label)
end

function logBiasPlottable(logsample::Array{Int64,1}, theta::Array{Float64,2}, theta_true::Array{Float64,1}; 
			color="red", linestyle="-", label="LabelMe")
	p,n = size(theta)
	bias = zeros(Float64, n)
	for i in 1:n
		residual = theta[:,i] .- theta_true
		ssquare = transpose(residual) * residual
		bias[i] = sqrt(ssquare[1]/p)
	end
	return Plottable(logsample, bias, color, linestyle, label)
end


function plot_costs(plottables::Array{Plottable,1}, png_filename::String; xlimits=(10^2, 10^6), ylimits=(1e-4, 1e4), dolegend=true, alpha=1.0, plot_title="", xlab="training size t", ylab="excess risk")
	clf()
	xlim(xlimits)
	ylim(ylimits)
	xlabel(xlab)
	ylabel(ylab)
	for method_pt in plottables
		loglog(method_pt.x, method_pt.cost, label=method_pt.label, color=method_pt.color, linestyle=method_pt.linestyle, alpha=alpha)
	end
	if dolegend
		legend(fontsize="small", loc="lower left")
	end
	if plot_title!=""
		title(plot_title)
	end
	savefig(png_filename, dpi=300)
end

function problem2a()
	#=function sgd_learning_rate(t::Int64)=#
	#=    ai::Float64=#
	#=    ai = (1+0.02*t)^(-1)=#
	#=    return ai=#
	#=end=#
	
	#=function good_learning_rate(t::Int64)=#
	#=    ai::Float64=#
	#=    ai = (1+0.02*t)^(-2/3)=#
	#=    return ai=#
	#=end=#
	
	#=function bad_learning_rate(t::Int64)=#
	#=    ai::Float64=#
	#=    ai = (1+t)^(-1/2)=#
	#=    return ai=#
	#=end=#

	sgd_learning_pars = LearningParameters(1.0, 0.02, 1.0)
	good_learning_pars = LearningParameters(1.0, 0.02, 2/3)
	bad_learning_pars = LearningParameters(1.0, 1.0, 1/2)
	
	n=10^6
	p = 100
	print("generating data")
	data = sample_data_2a(n,p)
	print("running implicit")
	theta_implicit = implicit_2a(data, sgd_learning_pars)
	implicit_plottable = logSamplePlottable(data,theta_implicit,
		"aqua","--","Implicit")
	print("running SGD")
	theta_sgd = sgd_2a(data, sgd_learning_pars)
	sgd_plottable = logSamplePlottable(data,theta_sgd,
		"red","-.","SGD")
	print("running good SGD")
	theta_good_sgd = sgd_2a(data, good_learning_pars)
	good_asgd_plottable = logSamplePlottable(data,asgd(theta_good_sgd),
		"magenta","-","ASGD")
	print("running bad SGD")
	theta_bad_sgd = sgd_2a(data, bad_learning_pars)
	bad_asgd_plottable = logSamplePlottable(data,asgd(theta_bad_sgd),
		"lime","-","ASGD_BAD")
	print("running batch")
	theta_batch = batch(data)
	batch_plottable = logSamplePlottable(data,theta_batch,
		"blue","--","Batch")
	print("computing costs")
	plot_costs([sgd_plottable, good_asgd_plottable, batch_plottable, bad_asgd_plottable, implicit_plottable], "mrischard_PS3_2a_plot1.png", ylimits=(1e-6, 1e2))
end

function batch_2b(data, logsample)
	p = data.p
	samples = length(logsample)
	theta_batch = zeros(Float64, p, samples)
	for i in 1:samples
		t = logsample[i]
		print(t)
		xtot = data.X[1:t,:]
		theta_batch[:,i] = pinv(xtot) * data.Y[1:t]
	end
	return theta_batch
end

function implicit_2b(X::Array{Float64,2}, Y::Array{Float64,2}, N::Int64, p::Int64, lpars::LearningParameters)
	theta_sgd::Array{Float64,2} = zeros(Float64, p, N)
	theta_old = vec(theta_sgd[:,1])
	@assert size(X) == (N,p)
	@assert size(Y) == (N,1)
	for t = 1:N
		@inbounds xi = vec(X[t,:]) # px1
		@inbounds yi = Y[t]
		ai::Float64 = learning_rate(lpars, t)
		lpred = dot(theta_old, xi)
		x_norm = sum(xi.^2)
		fi = 1 / (1 + ai * x_norm)
		theta_new = theta_old + (ai*yi - ai*fi*lpred - ai^2*fi*yi*x_norm)*xi
		theta_old = theta_new
		@inbounds theta_sgd[:,t] = theta_new
	end
	return theta_sgd
end

function implicit_2b(data::SampleData, lpars::LearningParameters)
	n = data.n
	p = data.p
	X = data.X
	Y = data.Y
	return implicit_2b(X, Y, n, p, lpars)
end


# problem 2b
function problem2b()
	n = 10^5
	p = 100
	data = sample_data_2b(n,p)
	logsample = int(floor(logspace(1,log10(n),100)))
	theta_batch = batch_2b(data, logsample)
	gamma0 = 1 / (sum(gen_lambdas_2b(p)))
	lambda0 = 0.01
	
	print("2b implicit\n")
	sgd_learning_rate =  LearningParameters(gamma0, lambda0, 1.0)
	asgd_learning_rate = LearningParameters(gamma0, lambda0, 2/3)
	theta_implicit = implicit_2b(data, sgd_learning_rate)
	print("2b sgd\n")
	theta_sgd = sgd_2b(data, sgd_learning_rate)
	theta_asgd = asgd(sgd_2b(data, asgd_learning_rate))
	
	sgd_plottable = logSamplePlottable(data, theta_sgd, "red", "-.", "SGD")
	asgd_plottable = logSamplePlottable(data, theta_asgd, "magenta", "-", "ASGD")
	batch_cost = get_cost(data.A, theta_batch, data.theta)
	batch_plottable = Plottable(logsample, batch_cost, "blue", "--", "Batch")
	implicit_plottable = logSamplePlottable(data, theta_implicit, "aqua", "--", "Implicit")
	plot_costs([sgd_plottable, asgd_plottable, implicit_plottable, batch_plottable], "mrischard_PS3_2b_plot1.png", xlimits=(1e2, 1e6), ylimits=(1e-4, 1e4))
end

# problem 2b
function problem2b_with_optim(sgd_methods::Array{SGDMethod,1})
	n = 10^6
	p = 100
	data = sample_data_2b(n,p)
	logsample = int(floor(logspace(1,log10(n),100)))
	theta_batch = batch_2b(data, logsample)
	gamma0 = 1 / (sum(gen_lambdas_2b(p)))
	lambda0 = 0.01
	
	plottables = Plottable[]
	print("2b implicit\n")
	for sgd_m in sgd_methods
		sgd_learning_rate = LearningParameters(sgd_m.opt_gamma, sgd_m.opt_a, sgd_m.c)
		theta_sgd = sgd_m.sgd(data, sgd_learning_rate)
		if sgd_m.name == "SGD"
			sgd_plottable = logSamplePlottable(data, theta_sgd, "red", "-.", "SGD")
		elseif sgd_m.name == "ASGD"
			sgd_plottable = logSamplePlottable(data, theta_sgd, "magenta", "-", "ASGD")
		elseif sgd_m.name == "Implicit"
			sgd_plottable = logSamplePlottable(data, theta_sgd, "aqua", "--", "Implicit")
		else
			throw(ArgumentError("sgd_m.name should be SGD/ASGD/Implicit"))
		end
		push!(plottables, sgd_plottable)
	end
	batch_cost = get_cost(data.A, theta_batch, data.theta)
	batch_plottable = Plottable(logsample, batch_cost, "blue", "--", "Batch")
	push!(plottables, batch_plottable)
	plot_costs(plottables, "mrischard_PS3_2b_plot2.png", xlimits=(1e2, 1e6), ylimits=(1e-4, 1e4))
end

function problem2c_explore()
	n = 10^6
	p = 100
	logsample = int(floor(logspace(1,log10(n),100)))
	gamma0 = 1 / (sum(gen_lambdas_2b(p)))
	lambda0 = 0.01
	repetitions = 20
	alphas = logspace(-5, 1, 20)
	plottables = Plottable[]
	_start = time()
	for rep in 1:repetitions
		#=costs = Array{=#
		data = sample_data_2b(n,p)
		for a in alphas
			print("2b implicit\n")
			sgd_learning_rate =  LearningParameters(gamma0, a, 1.0)
			asgd_learning_rate = LearningParameters(gamma0, a, 2/3)
			theta_implicit = implicit_2b(data, sgd_learning_rate)
			print("2b sgd\n")
			theta_sgd = sgd_2b(data, sgd_learning_rate)
			theta_asgd = asgd(sgd_2b(data, asgd_learning_rate))
			
			a_string = @sprintf(" a=%.3f", a)
			sgd_plottable = logSamplePlottable(data, theta_sgd, "red", "-", "")
			asgd_plottable = logSamplePlottable(data, theta_asgd, "magenta", "-", "")
			implicit_plottable = logSamplePlottable(data, theta_implicit, "aqua", "-", "")
			push!(plottables, sgd_plottable)
			push!(plottables, asgd_plottable)
			push!(plottables, implicit_plottable)
		end
		theta_batch = batch_2b(data, logsample)
		batch_cost = get_cost(data.A, theta_batch, data.theta)
		batch_plottable = Plottable(logsample, batch_cost, "blue", "-", "Batch")
		push!(plottables, batch_plottable)
		push!(plottables, batch_plottable)
		if time() - _start > 3600 # 1 hour
			break
		end
	end
	plot_costs(plottables, "mrischard_PS3_2c_plot1.png", xlimits=(1e2, 1e6), ylimits=(1e-4, 1e4), dolegend=false, alpha=0.05)
end

type ThetaSamplePlottable
	theta::Array{Float64,4}
	alphas::Array{Float64,1}
	logsample::Array{Int64,1}
	#=plotnum::Int64=#
	title::String
	A::Array{Float64,2}
end

function AverageTheta(bplot::ThetaSamplePlottable)
	p, nsamples, nalphas, repetitions = size(bplot.theta)
	avg_theta_sgd = Array(Float64, p, nsamples, nalphas)
	for ialpha in 1:nalphas
		for i in 1:nsamples
			for j in 1:p
				avg_theta_sgd[j,i,ialpha] = mean(bplot.theta[j,i,ialpha,:])
			end
		end
	end
	#=avg_theta_sgd      = mean(bplot.theta,4)[:,:,:,1]=#
	return avg_theta_sgd
end

function VarTheta(bplot::ThetaSamplePlottable)
	p, nsamples, nalphas, repetitions = size(bplot.theta)

	cov_theta_sgd = Array(Float64, p, p, nalphas, nsamples)
	trace_theta_sgd = Array(Float64, nalphas, nsamples)
	for ialpha in 1:nalphas
		for i in 1:nsamples
			this_cov = cov(transpose(slice(bplot.theta, :,i,ialpha,:)))
			cov_theta_sgd[:,:,ialpha,i] = this_cov
			trace_theta_sgd[ialpha,i] = trace(this_cov)
		end
	end
	return (cov_theta_sgd, trace_theta_sgd)
end

cmj = convert(Function, plt.get_cmap("gist_rainbow").o)
function get_color(i,ncolors)
	color_tuple = cmj(1.0*i/ncolors)
	r = convert(Int64, floor(color_tuple[1] * 255))
	g = convert(Int64, floor(color_tuple[2] * 255))
	b = convert(Int64, floor(color_tuple[3] * 255))
	colstring = @sprintf("#%02x%02x%02x", r, g, b)
	return colstring
end
	
function plot_2c(bplot::ThetaSamplePlottable, plotnum::Int64)
	p, nsamples, nalphas, repetitions = size(bplot.theta)
	theta_true = ones(Float64, p)
	plottables = Plottable[]
	avg_theta::Array{Float64,3} = AverageTheta(bplot)
	for ialpha = 1:nalphas
		a_string = @sprintf(" a=%.6f", bplot.alphas[ialpha])
		sgd_plottable = logBiasPlottable(bplot.logsample, avg_theta[:,:,ialpha], theta_true, 
			color=get_color(ialpha, nalphas), linestyle="-", label=a_string)
		push!(plottables, sgd_plottable)
	end
	plot_costs(plottables, 
		@sprintf("mrischard_PS3_2c_plot%d.png", plotnum),
		xlimits=(1e2, 1e6), 
		ylimits=(1e-3, 1e0), 
		dolegend=true,
		plot_title=bplot.title,
		ylab="bias",
	)
end

function plot_2d(bplot::ThetaSamplePlottable, plotnum::Int64)
	p, nsamples, nalphas, repetitions = size(bplot.theta)
	theta_true = ones(Float64, p)
	plottables = Plottable[]
	cov_theta::Array{Float64,4}, trace_theta::Array{Float64,2} = VarTheta(bplot)
	for ialpha = 1:nalphas
		a_string = @sprintf(" a=%.6f", bplot.alphas[ialpha])
		sgd_plottable = Plottable(bplot.logsample, slice(trace_theta, ialpha,:), 
			get_color(ialpha, nalphas), "-", a_string)
		push!(plottables, sgd_plottable)
	end
	plot_costs(plottables, 
		@sprintf("mrischard_PS3_2d_plot%d.png", plotnum),
		xlimits=(1e2, 1e6), 
		ylimits=(1e-4, 1e1), 
		dolegend=true,
		plot_title=bplot.title,
		ylab="var",
	)
end

#=function get_cost(A::Array{Float64,2}, theta_sgd::Array{Float64,2}, theta_true::Array{Float64,1})=#
function plot_costs(bplot::ThetaSamplePlottable, plotnum::Int64)
	p, nsamples, nalphas, repetitions = size(bplot.theta)
	theta_true = ones(Float64, p)
	plottables = Plottable[]
	ipl = 0
	for ialpha in 1:nalphas
		for rep in 1:repetitions
			ipl += 1
			a_string = @sprintf(" a=%.6f", bplot.alphas[ialpha])
			cost = get_cost(bplot.A, bplot.theta[:,:,ialpha,rep], theta_true)
			sgd_plottable = Plottable(bplot.logsample, cost, 
				get_color(ipl, nalphas*repetitions), "-", a_string)
			push!(plottables, sgd_plottable)
		end
	end
	plot_costs(plottables, 
		@sprintf("mrischard_PS3_2costs_plot%d.png", plotnum),
		xlimits=(1e2, 1e6), 
		ylimits=(1e-4, 1e4), 
		dolegend=false,
		plot_title=bplot.title,
		ylab="excess risk",
	)
end


function problem2c()
	repetitions = 30
	nalphas = 15
	n = 10^6
	p = 100
	alphas = logspace(-6.0, -1.0, nalphas)

	nsamples=110
	logsample = int(floor(logspace(1,log10(n),nsamples)))
	gamma0 = 1 / (sum(gen_lambdas_2b(p)))
	plottables = Plottable[]

	sample_theta_sgd      = zeros(Float64, p, nsamples, nalphas, repetitions)
	sample_theta_asgd     = zeros(Float64, p, nsamples, nalphas, repetitions)
	sample_theta_implicit = zeros(Float64, p, nsamples, nalphas, repetitions)

	lambdas,A,Q = random_covar(p)

	for rep in 1:repetitions
		data = sample_data_2b_from_A(n, p, A, Q, lambdas)
		for ialpha in 1:nalphas
			a = alphas[ialpha]

			sgd_learning_pars =  LearningParameters(gamma0, a, 1.0)
			asgd_learning_pars =  LearningParameters(gamma0, a, 2/3)

			sample_theta_sgd[:,:,ialpha,rep] =  sgd_2b(data, sgd_learning_pars)[:,logsample]
			sample_theta_asgd[:,:,ialpha,rep] = asgd(sgd_2b(data, asgd_learning_pars))[:,logsample]
			sample_theta_implicit[:,:,ialpha,rep] = implicit_2b(data, sgd_learning_pars)[:,logsample]
		end
	end
	
	theta_plottables = ThetaSamplePlottable[
		ThetaSamplePlottable(sample_theta_sgd, alphas,      logsample, "SGD",      A),
		ThetaSamplePlottable(sample_theta_asgd, alphas,     logsample, "ASGD",     A),
		ThetaSamplePlottable(sample_theta_implicit, alphas, logsample, "Implicit", A),
	]
	

	return theta_plottables
end




function question_2b_optimal()
	N = 5*10^4
	p = 100
	data = sample_data_2b(N,p)
	gamma0 = 1 / (sum(gen_lambdas_2b(p)))
	lambda0 = 0.01
	loga = -3
	theta_true = data.theta
	A = data.A
	
	
	sgd_method = SGDMethod(sgd_2b, "SGD", 1.0, lambda0, gamma0)
	asgd_method = SGDMethod(asgd_2b, "ASGD", 2/3, lambda0, gamma0)
	implicit_method = SGDMethod(implicit_2b, "Implicit", 1.0, lambda0, gamma0)
	
	for rep in 1:3
		for sgd_m in (sgd_method, asgd_method, implicit_method)
			sgd_function = sgd_m.sgd
			c = sgd_m.c
			function to_optim(loga::Float64)
				a = 10.0^loga
				sgd_learning_pars = LearningPars(sgd_m.opt_gamma, a, c)
				theta = sgd_function(data, sgd_learning_pars)
				last = theta[:,end]
				residual = last - theta_true
				square = transpose(residual) * A * residual
				return square[1]
			end
			o = Optim.optimize(to_optim, -10.0, -1.0)
			sgd_m.opt_a = 10^o.minimum
			print(sgd_m.name, o, "\n")
		end
		print("GAMMA\n")
		for sgd_m in (sgd_method, asgd_method, implicit_method)
			sgd_function = sgd_m.sgd
			c = sgd_m.c
			a = sgd_m.opt_a
			function to_optim(loggamma::Float64)
				gamma0 = 10.0^loggamma
				sgd_learning_pars = LearningPars(gamma0, a, c)
				theta = sgd_function(data, sgd_learning_pars)
				last = theta[:,end]
				residual = last - theta_true
				square = transpose(residual) * A * residual
				return square[1]
			end
			o = Optim.optimize(to_optim, -3.0, 1.0)
			sgd_m.opt_gamma = 10^o.minimum
			print(sgd_m.name, o, "\n")
		end
		print("sgd", sgd_method.opt_gamma, " ", sgd_method.opt_a)
		print("asgd", asgd_method.opt_gamma, " ", asgd_method.opt_a)
		print("implicit", implicit_method.opt_gamma, " ", implicit_method.opt_a)
	end
	return [sgd_method, asgd_method, implicit_method]
end

#=sgd_methods = question_2b_optimal()=#
#=problem2b_with_optim(sgd_methods)=#

#=function optimize(data::SampleData, min_a::Float64, max_a::Float64, =#


problem2a()
#=problem2b()=#

#=theta_plottables = problem2c();=#
#=for iplot in 1:length(theta_plottables)=#
	#=plot_costs(theta_plottables[iplot], iplot)=#
	#=plot_2d(theta_plottables[iplot], iplot+7)=#
#=    plot_2c(theta_plottables[iplot], iplot+7)=#
#=end=#

# ASGD
function plot_2e(theta_plottables::ThetaSamplePlottable)
	clf()
	
	for tpl in theta_plottables
		p, nsamples, nalphas, repetitions = size(tpl.theta)
		last_trace_covs = zeros(Float64, nalphas)
		for ialpha in 1:nalphas
			### EMPIRICAL
			last_theta_estimates = transpose(slice(tpl.theta,:,nsamples,ialpha,:))
			last_cov = cov(last_theta_estimates)
			last_trace = trace(last_cov)
			last_trace_covs[ialpha] = last_trace
		end
		loglog(tpl.alphas, last_trace_covs, "o", label=@sprintf("empirical %s", tpl.title))
	end
	
	sgd_plottable = theta_plottables[1]
	p, nsamples, nalphas, repetitions = size(tpl.theta)
	n = tpl.logsample[end]
	theoretical_sgd_covs = zeros(Float64, 100)
	theoretical_asgd_covs = zeros(Float64, 100)
	xalphas = logspace(-6,0,100)
	trA = sum(gen_lambdas_2b(p))
	Ainv_tr = sum(1.0 ./ gen_lambdas_2b(p))
	lambdas = gen_lambdas_2b(p)
	for (ialpha,alpha) in enumerate(xalphas)
		### THEORY
		ps_alpha = 1/alpha
		inbrackets = (2*ps_alpha*tpl.A - eye(p))
		#=theoretical_variance = ps_alpha^2 * inv(inbrackets) * tpl.A=#
		#=theoretical_trace = trace(theoretical_variance) / n=#
		theoretical_trace = ps_alpha^2 * sum(lambdas ./ ((2*ps_alpha)*lambdas .- 1.0)) / n
		theoretical_sgd_covs[ialpha]  = theoretical_trace
		theoretical_asgd_covs[ialpha] = Ainv_tr/n
	end
	
	loglog(xalphas, theoretical_sgd_covs, "-", color="blue", label="theoretical SGD")
	loglog(xalphas, theoretical_asgd_covs, "-", color="green", label="theoretical ASGD")
	xlabel("alpha")
	ylabel("variance")
	legend()
	
	png_filename = @sprintf("mrischard_PS3_2e_plot%d.png", 1)
	savefig(png_filename, dpi=300)
end
		
end
