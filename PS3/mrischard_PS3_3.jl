import Distributions
import HDF5
include("mrischard_PS3_2.jl")
using StatsBase

immutable SampleData3
	N::Int64
	p::Int64
	rho::Float64
	X::Array{Float64,2}
	Y::Array{Float64,1}
	beta_j::Array{Float64,1}
end

#=simulate_friedman_lasso_data <- function(N, p, rho){=#
#=    sigma <- (1-rho)*diag(p) + rho=#
#=    X <- rmvnorm(N, mean=rep(0,p), sigma=sigma)=#
#=    j <- 1:p=#
#=    beta_j <- (-1)^j * exp(-2*(j-1)/20)=#
#=    Y <- X %*% beta_j + rnorm(N)=#
#=    return(list(Y=Y, beta_j=beta_j,X=X,sigma=sigma))=#
#=}=#

function simulate_friedman_lasso_data(N::Int64, p::Int64, rho::Float64)
	#=sigma = (1-rho)*eye(p) .+ rho=#
	#=mvn = Distributions.MvNormal(sigma)=#
	Xiid = randn(N, p)

	a = sqrt(1-rho)
	b = (-a+sqrt((1-rho) + p*rho))/p
	if rho > 1e-10
		X = Array(Float64, size(Xiid)...)
		for (irow in 1:N)
			iid_row = Xiid[irow,:]
			X[irow,:] = a*iid_row + b*sum(iid_row)
		end
	else
		X = Xiid
	end

	j = float(1:p)
	beta_j = ((-1).^j) .* exp(-2.0.*(j-1.0)/20.0)
	epsilon = randn(N)
	y = X * beta_j + epsilon
	return SampleData3(N, p, rho, X, y, beta_j)
end


function save(filename::String, data::SampleData3)
	HDF5.h5write(filename, "X", data.X)
	HDF5.h5write(filename, "Y", data.Y)
	HDF5.h5write(filename, "beta_j", data.beta_j)
end



function gen_filename(N::Int64, p::Int64, rho::Float64)
	filename = @sprintf("cache/prob3_N%d_p%d_rho%.2f.h5", N, p, rho)
	return filename
end

function make_and_save(N::Int64, p::Int64, rho::Float64)
	data = simulate_friedman_lasso_data(N, p, rho)
	filename = gen_filename(N, p, rho)
	save(filename, data)
	return data
end
function load(N::Int64, p::Int64, rho::Float64)
	filename = gen_filename(N, p, rho)
	data = SampleData3(
		N,
		p,
		rho,
		HDF5.h5read(filename, "X"),
		HDF5.h5read(filename, "Y"),
		HDF5.h5read(filename, "beta_j")
	)
end
	

function saveall()
	#=fried_N = [1000, 5000, 100, 100, 100, 100]=#
	#=fried_p = [100, 100, 1000, 5000, 20000, 50000]=#
	fried_N = [10^5, 10^5, 10^6]
	fried_p = [100,  1000, 100]
	fried_type = ["naive", "covariance"]
	fried_correlation = [0, 0.1, 0.2, 0.5, 0.9, 0.95]
	for (index_np in 1:length(fried_N))
		N = fried_N[index_np]
		p = fried_p[index_np]
		for (rho in fried_correlation)
			make_and_save(N, p, rho)
		end
	end
end

fried_N = [1000, 5000, 100,  100,  100,   100,   10^5, 10^5, 10^6]
fried_p = [100,  100,  1000, 5000, 20000, 50000, 100,  1000, 100]
fried_correlation = [0, 0.1, 0.2, 0.5, 0.9, 0.95]
#=saveall()=#

function MSE(theta, theta_true)
	return sqrt(mean((theta-theta_true).^2))
end
repetitions = 2
sgd_mse = Array(Float64, length(fried_N), length(fried_correlation), repetitions)
asgd_mse = Array(Float64, length(fried_N), length(fried_correlation), repetitions)
implicit_mse = Array(Float64, length(fried_N), length(fried_correlation), repetitions)

for (index_np in 1:length(fried_N))
	N = fried_N[index_np]
	p = fried_p[index_np]
	sgd_timings = Float64[]
	asgd_timings = Float64[]
	implicit_timings = Float64[]

	@printf("\nN=%d p=%d\n", N, p)
	for (icor, rho) in enumerate(fried_correlation)
		data = load(N, p, rho)
		gamma0 = 1.0/p
		lambda0 = 1 - rho
		sgd_learning_rate =  Question2.LearningParameters(gamma0, lambda0, 1.0)
		asgd_learning_rate = Question2.LearningParameters(gamma0, lambda0, 2/3)
		tic()
		Y::Array{Float64,2} = zeros(Float64, (length(data.Y),1))
		Y[:] = data.Y
		for irep = 1:repetitions
			theta = Question2.sgd_2b(data.X, Y, data.N, data.p, sgd_learning_rate)
			@inbounds sgd_mse[index_np, icor,irep] = MSE(theta[:,end], data.beta_j)
		end
		sgd_time = toq()/repetitions
		push!(sgd_timings, sgd_time)
		tic()
		for irep = 1:repetitions
			theta = Question2.asgd(Question2.sgd_2b(data.X, Y, data.N, data.p, asgd_learning_rate))
			@inbounds asgd_mse[index_np, icor,irep] = MSE(theta[:,end], data.beta_j)
		end
		asgd_time = toq()/repetitions
		push!(asgd_timings, asgd_time)
		tic()
		for irep = 1:repetitions
			theta = Question2.implicit_2b(data.X, Y, data.N, data.p, sgd_learning_rate)
			@inbounds implicit_mse[index_np, icor,irep] = MSE(theta[:,end], data.beta_j)
		end
		implicit_time = toq()/repetitions
		push!(implicit_timings, implicit_time)
	end
	@printf("SGD & %.3f & %.3f & %.3f & %.3f & %.3f & %.3f \\\\ \n", sgd_timings[1], sgd_timings[2], sgd_timings[3], sgd_timings[4], sgd_timings[5], sgd_timings[6])
	@printf("ASGD & %.3f & %.3f & %.3f & %.3f & %.3f & %.3f \\\\ \n", asgd_timings[1], asgd_timings[2], asgd_timings[3], asgd_timings[4], asgd_timings[5], asgd_timings[6])
	@printf("Implicit & %.3f & %.3f & %.3f & %.3f & %.3f & %.3f \\\\ \n", implicit_timings[1], implicit_timings[2], implicit_timings[3], implicit_timings[4], implicit_timings[5], implicit_timings[6])
end

mean_mse_sgd = mean(sgd_mse, 3)*1000
mean_mse_asgd = mean(asgd_mse, 3)*1000
mean_mse_implicit = mean(implicit_mse, 3)*1000
for (index_np in 1:length(fried_N))
	N = fried_N[index_np]
	p = fried_p[index_np]
	@printf("\nN=%d p=%d\n", N, p)
	for (header, mean_mse) in ( ("SGD", mean_mse_sgd),
	                            ("ASGD", mean_mse_asgd),
	                            ("Implicit", mean_mse_implicit),
	                          )
		@printf("%10s & %6.1f & %6.1f & %6.1f & %6.1f & %6.1f & %6.1f \\\\ \n", 
			header,
			mean_mse[index_np, 1],
			mean_mse[index_np, 2],
			mean_mse[index_np, 3],
			mean_mse[index_np, 4],
			mean_mse[index_np, 5],
			mean_mse[index_np, 6],
		)
	end
end
		
