#!/bin/bash
#SBATCH -J test-testing # name for job array
#SBATCH -o all.out #Standard output
#SBATCH -e all.err #Standard error
#SBATCH -p general #Partition
#SBATCH -t 00:10:00 #Running time of 20 mins.
#SBATCH --mem-per-cpu 1000 #Memory request
#SBATCH -n 1 #Number of cores
#SBATCH -N 1 #All cores on one machine
#SBATCH --mail-type=END     #Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=mrischard@g.harvard.edu
#SBATCH --array 1-12

time Rscript main.timing.R
