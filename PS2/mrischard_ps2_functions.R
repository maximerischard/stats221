source("rASL.R")

simYgivenTheta <- function(theta, w, N){
	lambda <- matrix(theta * w)
	simulated <- apply(lambda, 1, function(l){rpois(N, l)})
	return(t(simulated))
}

simYgivenMuSigma2 <- function(mu, sigma2, w, N, nsim){
	nunits <- length(w)
	logtheta <- rnorm(n=nunits, mean=mu, sd=sqrt(sigma2))
	theta <- exp(logtheta)
	y <- replicate(nsim, simYgivenTheta(theta, w, N))
	return(list(logtheta=logtheta, Y=y))
}

extract_statistics <- function(mcmc_output){
	logTheta <- mcmc_output$logTheta
	mu <- mcmc_output$mu
	sigmasq <- mcmc_output$sigmasq
	
	quantiles <- c(0.025, 0.05, 0.16, 0.32, 0.5, 0.68, 0.84, 0.95, 0.975)
	logTheta.quantiles <- t(apply(logTheta, 1, function(row){quantile(row, quantiles)}))
	logTheta.mean <- apply(logTheta, 1, mean)
	logTheta.sd <- apply(logTheta, 1, sd)
	mu.quantiles <- quantile(mu, quantiles)
	sigmasq.quantiles <- quantile(sigmasq, quantiles)
	return( list(
		logTheta.quantiles = logTheta.quantiles,
		logTheta.mean = logTheta.mean,
		logTheta.sd = logTheta.sd,
		mu.quantiles = mu.quantiles,
		sigmasq.quantiles = sigmasq.quantiles
	))
}

getting_mu_sigma_output <- function(task, mu, sigma){
	pattern <- sprintf("mu_%.1f_sigma_%.1f_.*\\.Rdata$", mu, sigma)
	path <- sprintf("task%d_output", task)
	Rdata_files <- dir(path=path, pattern=pattern, full.names=TRUE)
	return(Rdata_files)
}

getting_m_b_output <- function(task, m, b){
	pattern <- sprintf("m_%.1f_b_%.1f_.*\\.Rdata$", m, b)
	path <- sprintf("task%d_output", task)
	Rdata_files <- dir(path=path, pattern=pattern, full.names=TRUE)
	return(Rdata_files)
}

build_confidence_dframe <- function(Rdata_files){
	in_confidence_dframe <- data.frame(logTheta.true=numeric(), 
			w=numeric(),
			median.theta=numeric(),
			in.68=logical(), 
			in.95=logical())
	
	
	for (fname in Rdata_files){
		load(fname)
		
		in_confidence <- function(output, interval){
			logTheta.quantiles <- output$logTheta.quantiles
			logTheta.true <- output$logtheta
		
			lower.bound <- paste((100-interval)/2, "%", sep="")
			upper.bound <- paste(100-(100-interval)/2, "%", sep="")
		
			lower.quantile <- logTheta.quantiles[,lower.bound]
			upper.quantile <- logTheta.quantiles[,upper.bound]
		
			above.lower <- logTheta.true > lower.quantile
			below.upper <- logTheta.true < upper.quantile
			in.conf.int <- above.lower & below.upper
			return(in.conf.int)
		}
		
		in68 <- in_confidence(output, 68)
		in95 <- in_confidence(output, 95)
		median <- output$logTheta.quantiles[,"50%"]
		logTheta.true <- output$logtheta
		w <- output$w
		in_confidence <- data.frame(logTheta.true=logTheta.true, 
			median.theta=median, 
			w=w, 
			in.68 = in68, 
			in.95=in95)
		in_confidence_dframe <- rbind(in_confidence_dframe, in_confidence)
	}	
	return(in_confidence_dframe)
}

binned_confidence_intervals <- function(x, in.confint, nbins=100){
	logTheta.quantiles <- quantile(x, seq(0,1,length.out=nbins)[2:nbins-1])
	breaks <- c(min(x)-1, logTheta.quantiles, max(x)+1)
	logTheta.bins <- cut(x, breaks)
	mean.theta <- tapply(x, logTheta.bins, mean)
	mean.in.confint  <- tapply(in.confint, logTheta.bins, mean)
	return (list(mean.theta=mean.theta, mean.in.confint=mean.in.confint))
}

plot_confidence_intervals <- function(in_confidence_dframe){
	x <- in_confidence_dframe$logTheta.true
	binned.in.68 <- binned_confidence_intervals(x, in_confidence_dframe$in.68)
	binned.in.95 <- binned_confidence_intervals(x, in_confidence_dframe$in.95)
	mean.theta <- binned.in.68$mean.theta
	mean.in.68 <- binned.in.68$mean.in.confint
	mean.in.95 <- binned.in.95$mean.in.confint
	plot(mean.theta, mean.in.68, pch=1,
		xlab = "log(theta)",
		ylab = "Estimated Coverage",
		xlim=c(-4, 10),
		ylim=c(0, 1.0))
	points(mean.theta, y=mean.in.95, pch=2)
	lines(ksmooth(x, in_confidence_dframe$in.68, kernel="box", bandwidth=1.0, range=range(mean.theta)), col="red")
	lines(ksmooth(x, in_confidence_dframe$in.95, kernel="box", bandwidth=1.0, range=range(mean.theta)), col="red")
	abline(h=0.95, col=4, lty=2)
	abline(h=0.68, col=4, lty=2)
}

plot_weights <- function(in_confidence_dframe){

	x <- log(in_confidence_dframe$w) ## THAT'S THE MAIN LINE THAT CHANGES
	binned.in.68 <- binned_confidence_intervals(x, in_confidence_dframe$in.68)
	binned.in.95 <- binned_confidence_intervals(x, in_confidence_dframe$in.95)
	mean.theta <- binned.in.68$mean.theta
	mean.in.68 <- binned.in.68$mean.in.confint
	mean.in.95 <- binned.in.95$mean.in.confint
	plot(mean.theta, mean.in.68, pch=1,
		xlab = "log(weight)",
		ylab = "Estimated Coverage",
		xlim=range(x),
		ylim=c(0, 1.0))
	points(mean.theta, y=mean.in.95, pch=2)
	lines(ksmooth(x, in_confidence_dframe$in.68, kernel="box", bandwidth=1.0, range=range(mean.theta)), col="red")
	lines(ksmooth(x, in_confidence_dframe$in.95, kernel="box", bandwidth=1.0, range=range(mean.theta)), col="red")
	abline(h=0.95, col=4, lty=2)
	abline(h=0.68, col=4, lty=2)
}


fracSec <- function() {
	now <- as.vector(as.POSIXct(Sys.time())) / 1000
	as.integer(abs(now - trunc(now)) * 10^8)
}

run_sims <- function(task, mu, sigma, N, nthetadraws, nsimpertheta, w, array_id){
	nthetas <- length(w)

	for (idraw in 1:nthetadraws){
		logtheta <- rnorm(n=nthetas, mean=mu, sd=sigma)
		theta <- exp(logtheta)
		for (isim in 1:nsimpertheta){
			y <- simYgivenTheta(theta, w, N)
			p <- poisson.logn.mcmc(y, w)
			mcmc.processed = extract_statistics(p)
			output = list(
				y=y, 
				logtheta=logtheta, 
				mu=mu, 
				sigma=sigma, 
				nsimpertheta=nsimpertheta, 
				N=N, 
				nthetas=nthetas, 
				w=w
			)
			output = c(output, mcmc.processed)
			save(output, file=sprintf("task%d_output/mu_%.1f_sigma_%.1f_array_%d_ithetadraw_%d_isim_%d.Rdata", 
					task, 
					mu, 
					sigma, 
					array_id, 
					idraw, 
					isim))
		}
	}
}

misspecified_sims <- function(task, m, b, N, nthetadraws, nsimpertheta, w, array_id){
	nthetas <- length(w)

	for (idraw in 1:nthetadraws){
		logtheta <- rASL(n=nthetas, x0=1.6, m=m, b=b)
		theta <- exp(logtheta)
		for (isim in 1:nsimpertheta){
			y <- simYgivenTheta(theta, w, N)
			p <- poisson.logn.mcmc(y, w)
			mcmc.processed = extract_statistics(p)
			output = list(
				y=y, 
				logtheta=logtheta, 
				m=m,
				b=b,
				nsimpertheta=nsimpertheta, 
				N=N, 
				nthetas=nthetas, 
				w=w
			)
			output = c(output, mcmc.processed)
			save(output, file=sprintf("task%d_output/m_%.1f_b_%.1f_array_%d_ithetadraw_%d_isim_%d.Rdata", 
					task, 
					m, 
					b, 
					array_id, 
					idraw, 
					isim))
		}
	}
}

plot_misspecified_prior <- function(x0, m, b){
	x <- seq(-20,20,0.1)
	y <- rASL(10000, x0, m, b)
	hist(y,
		breaks=x, 
		xlim=c(-10,10), 
		freq=FALSE, 
		ylim=c(0,0.7),
		main=sprintf("Histogram of rASL(%d, x0=%.1f, m=%.1f, b=%.1f)", 10000, x0, m, b)
	)
	y.mean <- mean(y)
	y.sd <- sd(y)
	fitted.normal <- dnorm(x, mean=y.mean, sd=y.sd)
	lines(x, fitted.normal, col="red")
	text(x=y.mean+1, y=max(fitted.normal), labels=sprintf("Normal(mu=%.1f, sigma=%.1f)", 
		y.mean, y.sd), col="red", pos=4, cex=0.4)
}

save_misspecified_prior_plots <- function(){
	m_var <- c(0, -0.7, 0.7, 0)
	b_var <- c(1.3, 1.3, 1.3, 2.6)
	x0 = 1.6
	task <- 5
	for (i.mu.sigma in 1:4){
		m <- m_var[i.mu.sigma]
		b <- b_var[i.mu.sigma]
		png(sprintf("mrischard_ps2_task%d_plot%d.png", task, i.mu.sigma+8), width=1000, height=1000, res=200)
		plot_misspecified_prior(x0, m, b)
		dev.off()
	}
}
	

seed <- fracSec()
set.seed(seed)
