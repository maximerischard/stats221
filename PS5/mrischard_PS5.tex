%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Short Sectioned Assignment
% LaTeX Template
% Version 1.0 (5/5/12)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Frits Wenneker (http://www.howtotex.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass[paper=a4, fontsize=11pt]{scrartcl} % A4 paper and 11pt font size

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\usepackage{fourier} % Use the Adobe Utopia font for the document - comment this line to return to the LaTeX default
\usepackage[english]{babel} % English language/hyphenation
\usepackage{amsmath,amsfonts,amsthm,commath} % Math packages
\usepackage{graphicx}
\usepackage[]{algorithm2e}
\DeclareMathOperator{\Tr}{Tr}
\DeclareMathOperator{\Var}{Var}
\DeclareMathOperator{\Exp}{E}
\DeclareMathOperator{\Cov}{Cov}

\usepackage{lipsum} % Used for inserting dummy 'Lorem ipsum' text into the template

\usepackage{sectsty} % Allows customizing section commands
\allsectionsfont{\centering \normalfont\scshape} % Make all sections centered, the default font and small caps

\usepackage{fancyhdr} % Custom headers and footers
\pagestyle{fancyplain} % Makes all pages in the document conform to the custom headers and footers
\fancyhead{} % No page header - if you want one, create it in the same way as the footers below
\fancyfoot[L]{} % Empty left footer
\fancyfoot[C]{} % Empty center footer
\fancyfoot[R]{\thepage} % Page numbering for right footer
\renewcommand{\headrulewidth}{0pt} % Remove header underlines
\renewcommand{\footrulewidth}{0pt} % Remove footer underlines
\setlength{\headheight}{13.6pt} % Customize the height of the header

\numberwithin{equation}{section} % Number equations within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)
\numberwithin{figure}{section} % Number figures within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)
\numberwithin{table}{section} % Number tables within sections (i.e. 1.1, 1.2, 2.1, 2.2 instead of 1, 2, 3, 4)


%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height

\title{	
\normalfont\normalsize
\textsc{Harvard Statistics 221} \\ [25pt] % Your university, school and/or department name(s)
\horrule{0.5pt} \\[0.4cm] % Thin top horizontal rule
\huge problem set no. 5\\ % The assignment title
\horrule{2pt} \\[0.5cm] % Thick bottom horizontal rule
}

\author{Maxime Rischard} % Your name

\date{\normalsize\today} % Today's date or a custom date
\newcommand{\ntheta}{N_\theta}

\begin{document}

\maketitle % Print the title

\section{Reproducing Cao et al.}

\subsection{Plotting link loads}

\includegraphics[width=\textwidth]{mrischard_fig2.pdf}

\subsection{Plotting variance-mean relationship}

\includegraphics[width=\textwidth]{mrischard_fig4.pdf}

\subsection{Derivation of the IID EM algorithm}

In the model developed by Cao et al., $X$ is multivariate normal with diagonal covariance matrix,
$Y$ is a deterministic function of $X$: $Y=AX$, where A is the transfer matrix that attributes
traffic from any OD pair in $X$ to the marginal inputs and outputs of $Y$. 
Lastly, $X$ has mean $\lambda$, and its covariance is parametrized as $\Sigma_{ii} = \phi \lambda^c$.
From Figure~4, we deduced that $c=2$ is a reasonable choice (which is suggestive of a Gamma distribution
 but the paper sticks to a normal model). $X$ and $Y$ together are multivariate normal, so
 we can write:
 \begin{align*}
	\Cov\del{X,X} &= \Var\del{X} = \Sigma \\
	\Cov\del{Y,Y} &= \Cov\del{AX,AX} = A\Cov\del{X,X}A' = A\Sigma A' \\
	\Cov\del{Y,X} &= \Cov\del{AX,X} = A\Cov\del{X,X} = A\Sigma \\
	\Cov\del{X,Y} &= \Cov\del{Y,X}' = \Sigma A' \\
	\begin{pmatrix} X \\ Y \end{pmatrix} &= \mathcal{N}\del{
		\begin{pmatrix} \lambda \\ A\lambda \end{pmatrix}, % mean
		\begin{bmatrix} \Sigma & \Sigma A' \\ A \Sigma & A \Sigma A' \end{bmatrix}
		}
 \end{align*}
 The log-likelihood $l(\theta|X)$ follows as the sum over all the data $t=1 \cdots T$ of the normal likelihood 
 for $X \sim \mathcal{N}\del{\lambda, \Sigma}$:
 \begin{equation*}
	l\del(\theta|X) = -\frac{T}{2} \log \left| \Sigma \right| - \frac{1}{2} \sum_{t=1}^T{\del{x_t - \lambda} \Sigma^{-1} \del{x_t - \lambda}}
 \end{equation*}
 To find the $Q(\theta, \theta^{(k)}$ function, the EM algorithm requires us to take the expectation of the above likelihood given the observed data $Y$ and the previous parameters $\theta^{(k)}$. Using standard results for multivariate normals, we can write:
 \begin{align*}
	\Exp\del{X|Y,\theta} &= EX + \Sigma_{XY} \Sigma_{YY}^{-1} \del{Y - EY} \\
	           &= \lambda + \Sigma A' \del{A\Sigma A'}^{-1} \del{Y - A\lambda} \\
	\Var\del{X|Y,\theta} &= \Var{X} - \Sigma_{XY} \Sigma_{YY}^{-1} \Sigma_{YX} \\
	             &= \Sigma - \Sigma A' \del{A\Sigma A'}^{-1} A \Sigma \\
\end{align*}
We can use these results to derive $\Exp\del{l\del{\theta|X}|Y,\theta^{(k)}}$. 
Since $l\del{\theta|X}$ is a quadratic form of $X$, we can use:
\begin{align*}
	\Exp\sbr{\epsilon' \Lambda \epsilon} &= \Tr{\sbr{\Lambda\Sigma}} + \mu' \Lambda \mu \\
	\Exp\sbr{\del{x_t - \lambda} \Sigma^{-1} \del{x_t - \lambda}|Y,\theta^{(k)}} &= 
		\Tr{\sbr{\Sigma^{-1}\Var\del{X|Y,\theta^{(k)}}} }
		+ \del{\Exp\del{X|Y,\theta^{(k)}} - \lambda}' \Sigma^{-1} \del{\Exp\del{X|Y,\theta^{(k)}} - \lambda}' \\
\end{align*}
We now sum over the $t$ data, adopting Cao et al.'s abbreviations $m_t^{(k)} \equiv \Exp\del{X|Y,\theta^{(k)}}$ and $R^{(k)} \equiv \Var\del{X|Y,\theta^{(k)}}$ (note $R$ does not have a $t$ index
because $\Var\del{X|Y,\theta}$ does not depend on $Y$). 
Thus we obtain the form of the $Q$ function from Cao et al.:
\begin{equation*}
	Q\del{\theta,\theta^{(k)}} = -\frac{T}{2} \del{\log \left| \Sigma \right|  + \Tr\del{\Sigma^{-1} R^(k)}}
	 -\frac{1}{2} \sum_{t=1}^T{ \del{m_t^{(k)} - \lambda}' \Sigma^{-1} \del{m_t^{(k)} - \lambda}}
\end{equation*}


We now have all the machinery necessary for the EM algorithm. The equations for $\Exp\del{X|Y,\theta}$ and $\Var\del{X|Y,\theta}$, with $\theta$ suitably index by $k$ to indicate conditioning on the previous step's $\theta$ estimate, give us the E step. 
The M step is then simply a matter of maximizing $Q\del{\theta,\theta^{(k)}}$ as given above. In my implementation, I used a standard L-BFGS algorithm to which I fed gradients obtained by differentiating $Q$ by $\theta$. 
We can write down the derivatives explicitly in terms of $\lambda$, $\phi$ $m_t^{(k)}$ and $R^{(k)}$ only.
\begin{align*}
	\pd{Q}{\lambda_i} &= \frac{-T}{2} \sbr{
		\frac{c}{\lambda_i}  % log
		- \frac{c}{\phi} * \frac{R_{ii}^{(k)}}{\lambda_i^{c+1}}}  % trace
		+ \frac{1}{\phi} \sum_{t=1}^T{  % deriv var
			\cbr{ \frac{c}{2}\frac{\del{\lambda_i - m_{t,i}^{(k)}}^2}{\lambda^{c+1}}  % product rule
			    - \frac{\del{\lambda_i - m_{t,i}^{(k)}}}{\lambda^{c}} } } \\
	\pd{Q}{\phi} &= \frac{-T \times I}{2 \phi} % log
	              + \frac{T}{2} \sum_{i=1}^I\cbr{\frac{R_{ii}^{(k)}}{\phi \lambda^c}} % trace
	              + \frac{1}{2 \phi^2} \sum_{i=1}^I \sum_{t=1}^T 
					\cbr{\frac{\del{\lambda_i - m_{t,i}^{(k)}}^2}{\lambda^{c}}}
\end{align*}
Where $T$ is the number of times (samples), and $I$ is the number of nodes. To be explicit, here are the steps of the EM algorithm:

\begin{algorithm}[H]
\DontPrintSemicolon
\KwData{$Y_t$ the measurements of network activity to and from each node at times $t=1 \cdots T$} \;
$c \longleftarrow 2$ (for example) \;
$k \longleftarrow 0$ \;
Set $\lambda^{(0)}$ \;
Set $\phi^{(0)}$ \;
$\theta^{(0)} \longleftarrow \sbr{\lambda^{(0)}; \phi^{(0)}}$ \;
\While{the likelihood hasn't converged}{
	Perform the E step by computing $m_t^{(k)} \forall t=1 \cdots T$ and $R^{(k)}$ \;
	Perform the M step by maximizing $Q\del{\theta,\theta^{(k)}}$ as a function of $\theta$ \;
	$\theta^{(k+1)} \longleftarrow $ the value of $\theta$ that maximizes $Q$ \;


	$k \longleftarrow k+1$ \;
	}
\end{algorithm}

We use some heuristics to set $\lambda^{(0)}$ and $\phi^{(0)}$. Their values do not significantly
impact the output of the algorithm.

\subsection{Locally IID model}
The EM algorithm above can be adapted to find a local fit of $\theta$.

\begin{algorithm}[H]
\DontPrintSemicolon
\KwData{$Y_t$ the measurements of network activity to and from each node at times $t=1 \cdots T$}\;
$c \longleftarrow 2$ (for example) \;
$w \longleftarrow $ the desired width of the local window \;
\For{$t = w/2+1 \cdots T - w/2$}{
	$Y_{local} \longleftarrow \cbr{Y_{t-w/2}, Y_{t-w/2+1}, \cdots, Y_{t+w/2-1}, Y_{t+w/2}}$\;
	$k \longleftarrow 0$ \;
	Set $\lambda^{(0)}$ \;
	Set $\phi^{(0)}$ \;
	$\theta_t^{(0)} \longleftarrow \sbr{\lambda^{(0)}; \phi^{(0)}}$ \;
	\While{the likelihood hasn't converged}{
		Perform the E step by computing $m_\tau^{(k)} \forall \tau=t-w/2+1 \cdots t+w/2$ and $R^{(k)}$, conditioning on $Y_{local}$ instead of the full $Y$ data. \;
		Perform the M step by maximizing $Q\del{\theta_t,\theta_t^{(k)}}$, using $w$ instead of $T$. \;
		$\theta_t^{(k+1)} \longleftarrow $ the value of $\theta$ that maximizes $Q$ at $t$. \;
		$k \longleftarrow k+1$ \;
		}
	Store the local $\theta_t$ output. \;
}
\end{algorithm}

My results (see Figure) match those of Cao at Al. closely, though not exactly. Where they disagree
, my output are closer to the measurements of $X$. While implementing the algorithm,
it was (painfully) clear that there were a lot of numerical instabilities in this problem,
so it is possible that my implementation is simply a bit more stable than Cao et al.'s,
explaining the discrepancies. One thing I do is that I always feed the previous window's $\theta$
fit as the initial guess for the fit of the next. I did this to accelerate convergence, but it
might be that doing so improved the stability of the algorithm.

\includegraphics[width=\textwidth]{mrischard_fig5_router1.pdf}

\subsection{Refined Model}

\includegraphics[width=\textwidth]{mrischard_fig6_router1.pdf}

\subsection{Comparing Tebaldi \& West (1998)}

The quality of the model of Tebaldi \& West (1998) is quite difficult to compare to that of Cao et al.
because they do not actually analyze the same data from the same network. As pointed out by Cao and by Vardi in his comment on Tebaldi, the main weakness of Tebaldi is that they only look at one sample ($t$) at a time. Consequently, they don't take advantage of all the available information. This information is captured in Cao et al. both by the moving window (pooling data from 11 adjacent times to obtain an estimate) and by the explicit smoothing enforced by the “refined” model. 

That being said, adding smoothness to Tebaldi et al.'s model wouldn't be hugely difficult 
(at least theoretically;
in practice it might take a long time to obtain estimates using their simulation procedure) by
adding a Markovian transition term (i.e. a prior on the change in parameters from one time to the next) 
just like Cao et al. do in their refined model.

The other main difference between the models is that Tebaldi et al. assume a Poisson model for $X$. In their
exploratory analysis, I think Cao et al. show pretty conclusively that that isn't a very good
model for the data, since variance scales at least as the square of the mean (as shown in Figure~4), which
is why they parametrize $X$ as normal with variance scaling as the square of the mean.
Consequently, I would expect Cao to outperform Tebaldi even after introducing smoothing to 
Tebaldi's model.

\subsection{Second Network (\texttt{2router\_linkcount.dat}) }

\includegraphics[width=\textwidth]{mrischard_fig5_router2.pdf}

\includegraphics[width=\textwidth]{mrischard_fig6_router2.pdf}



\end{document}
