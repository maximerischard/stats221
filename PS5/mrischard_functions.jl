using DataFrames
using Optim
using Dates
using PyPlot

immutable SigmaConstraints
	c::Union(Int, Float64)
	ϕ::Float64
end

immutable XParams
	λ::Array{Float64,1}
	Σ::Diagonal{Float64}
end

immutable OptimParams
	λ::Array{Float64,1}
	ϕ::Float64
end

function likelihood(θ::OptimParams, y_arr::Array{Float64,2}, A::Array{Int,2}, c::Int)
	ny, ntimes = size(y_arr)
	λ = θ.λ

	constr = SigmaConstraints(c, θ.ϕ)
	params = XParams(λ, constr)
	Σ = params.Σ

	y_var = A * Σ * transpose(A)
	y_var_inv = inv(y_var)
	loglik_ss = 0.0
	prediction = A*λ
	for t = 1:ntimes
		diff = y_arr[1:end-1,t] .- prediction
		loglik_ss += transpose(diff) * y_var_inv * diff
	end
	loglik = -ntimes/2 * log(det(y_var)) - 1/2 * loglik_ss
	return loglik
end

function XParams(λ::Array{Float64,1}, constr::SigmaConstraints)
	Σ=Diagonal(constr.ϕ*(λ.^constr.c))
	return XParams(λ, Σ)
end

function read_router(datafile::String)
	router = DataFrames.readtable(datafile)
	router[:time] = [d+Dates.Year(1900) for d in DateTime(router[:time], "(mm/dd/yy HH:MM:SS)")]
	pool!(router, [:nme, :method]) # turn into factors
	sort!(router, cols= [:time, :nme])
	#=router$time <- strptime(router$time, format="(%m/%d/%y %H:%M:%S)")=#
	return router
end


function conditional_mean(y_t::DataArray{Float64,1}, 
							prev_params::XParams,
							A::Array{Int,2})
	projection_mat = projection_matrix(prev_params.Σ, A)
	return conditional_mean(y_t, prev_params, projection_mat, A)
end

function conditional_mean(y_t::AbstractArray{Float64,1}, 
							prev_params::XParams,
							projection_mat::Array{Float64,2},
							A::AbstractArray{Int,2})
	y_trimmed = y_t[1:end-1]
	y_mean = A*prev_params.λ
	new_mean = prev_params.λ + projection_mat * (y_trimmed - y_mean)
	return new_mean
end


function projection_matrix(Σ::AbstractArray{Float64,2}, A::AbstractArray{Int,2})
	y_var = A * Σ
	xy_covar = A * Σ * transpose(A)
	return transpose(y_var) * inv(xy_covar)
end

function conditional_variance(prev_params::XParams,
							projection_mat::Array{Float64,2},
							A::AbstractArray{Int,2})
	prev_Σ = prev_params.Σ
	y_var = A * prev_Σ
	return prev_Σ - projection_mat * y_var
end

function time_snapshot(router_df::DataFrame, time::DateTime) # type of DataFrame?
	return router_df[router_df[:time].==time, :]
end

function extract_yt(snapshot_df::AbstractDataFrame, nodes::Vector{ASCIIString}; 
                    src_prefix="src", dst_prefix="dst")
	src = ASCIIString[src_prefix*" "*n for n in nodes]
	dst = ASCIIString[dst_prefix*" "*n for n in nodes]
	src_indices = Int64[searchsorted(snapshot_df[:nme], label)[1] for label in src]
	dst_indices = Int64[searchsorted(snapshot_df[:nme], label)[1] for label in dst]
	indices = [src_indices; dst_indices]
	return snapshot_df[indices,:value]
end

function extract_yt(values_arr::Array{Float64,2}, cnames::Vector{String}, nodes::Vector{ASCIIString};
                    src_prefix="src", dst_prefix="dst")
	src = ASCIIString[src_prefix*" "*n for n in nodes]
	dst = ASCIIString[dst_prefix*" "*n for n in nodes]
	src_indices = Int64[searchsorted(cnames, label)[1] for label in src]
	dst_indices = Int64[searchsorted(cnames, label)[1] for label in dst]
	y_indices = [src_indices; dst_indices]
	return values_arr[y_indices,:]
end

function extract_xt(snapshot_df::AbstractDataFrame, edges::Vector{ASCIIString})
	edge_indices = Int64[searchsorted(snapshot_df[:nme], label)[1] for label in edges]
	return snapshot_df[edge_indices,:value]
end

function extract_xt(values_arr::Array{Float64,2}, cnames::Vector{String}, edges::Vector{ASCIIString})
	edge_indices = Int64[searchsorted(cnames, label)[1] for label in edges]
	return values_arr[edge_indices,:]
end

function transfer_matrix(nodes::Vector{ASCIIString}, edges::Vector{(ASCIIString,ASCIIString)})
	transf = zeros(Int64, length(nodes)*2, length(edges))
	for (isrc, src) in enumerate(nodes)
		for (iedge, edge) in enumerate(edges)
			if src == edge[1]
				transf[isrc, iedge] = 1
			end
		end
	end
	for (isrc, src) in enumerate(nodes)
		for (iedge, edge) in enumerate(edges)
			if src == edge[2]
				transf[isrc+length(nodes), iedge] = 1
			end
		end
	end
	return transf
end

function extract_mt_from_snapshot(snapshot_df::AbstractDataFrame, prev_params::XParams, 
		projection_mat::Array{Float64,2},
		A::AbstractArray{Int,2},
		)
	y_t = extract_yt(snapshot_df)
	#=x_t = extract_xt(snapshot)=#
	#=@assert sum(abs(y_t-transfer*x_t)) < 1=#
	m_t = conditional_mean(y_t, prev_params, projection_mat, A)
	return m_t
end

function iterate_conditional_means(router_df::DataFrame, 
		prev_params::XParams, constr::SigmaConstraints,
		projection_mat::Array{Float64,2},
		A::AbstractArray{Int,2},
		)
	grouped_router = groupby(router_df, :time)
	ntimes = length(grouped_router)
	nedges = size(A)[2]
	conditional_means = Array(Float64, nedges, ntimes)
	for (itime,snap) in enumerate(grouped_router)
		conditional_means[:,itime] = extract_mt_from_snapshot(snap, prev_params, projection_mat, A)
	end
	return conditional_means
end

function iterate_conditional_means(y_arr::Array{Float64,2},
		prev_params::XParams, constr::SigmaConstraints,
		projection_mat::Array{Float64,2},
		A::Array{Int,2},
		)
	ny, ntimes = size(y_arr)
	nedges = size(A)[2]
	conditional_means = Array(Float64, nedges, ntimes)
	for itime = 1:ntimes
		conditional_means[:,itime] = conditional_mean(y_arr[:,itime], prev_params, projection_mat, A)
	end
	return conditional_means
end

function pivot_router(router_df::DataFrame)
end

function eval_Q(params::XParams,
		conditional_var::Array{Float64,2}, 
		conditional_means::Array{Float64,2},
		)
	nedges, ntimes = size(conditional_means)
	Σ_inv = inv(params.Σ)
	var_mean = 0.0
	for t = 1:ntimes
		m_t = conditional_means[:,t]
		diff = m_t - params.λ
		for i = 1:nedges
			var_mean += diff[i]^2 / params.Σ[i,i]
		end
		#=var_mean += transpose(diff) * Σ_inv * diff=#
	end
	R = conditional_var
	# should be able to speed this up by taking advantage of inv(sigma) being diagonal
	#=@assert abs(det(params.Σ) - prod(diag(params.Σ))) < 1=#
	#=print("diag", diag(params.Σ), "\n")=#
	log_det_Σ = sum(log(diag(params.Σ)))
	#=@assert abs(trace(Σ_inv * R) - sum(diag(R) ./ diag(params.Σ))) < 1e-5=#
	tr = sum(diag(R) ./ diag(params.Σ))
	#=print("Q calc -- det_Σ:", det_Σ, ";tr=", tr, "var_mean=", var_mean,"\n")=#
	Q = -ntimes/2 * ( log_det_Σ + tr) - 1/2 * var_mean 
	return Q[1]
end	

function Q_λ_gradient(
		λ::Vector{Float64},
		ϕ::Float64,
		c::Int,
		r_ii::Vector{Float64},
		ntimes::Int,
		nedges::Int,
		tr::Float64,
		dist_from_mean::Array{Float64,2},
		)
	deriv_log = c ./ λ
	#=print("deriv_log=", -ntimes / 2 * deriv_log, "\n")=#
	deriv_trc = (-c / ϕ) .* r_ii .* λ.^(-(c+1))
	#=print("deriv_trc=", -ntimes / 2 * deriv_trc, "\n")=#
	#=deriv_var =  1/ϕ * sum(c .* (dist_from_mean.^2) ./ λ.^(c+1) - (dist_from_mean ./ λ.^c), 2)=#
	deriv_var =  (1/ϕ) .* sum((c/2) .* (dist_from_mean.^2) ./ λ.^(c+1) .- (dist_from_mean ./ λ.^c), 2)
	#=print("deriv_var=", deriv_var, "\n")=#
	deriv_λ = (-ntimes/2) .* (deriv_log .+ deriv_trc) .+ deriv_var
	return deriv_λ
end

function Q_λ_second(
		λ::Vector{Float64},
		ϕ::Float64,
		c::Int,
		r_ii::Vector{Float64},
		ntimes::Int,
		nedges::Int,
		tr::Float64,
		dist_from_mean::Array{Float64,2},
		)
	λ_c   = λ.^c
	λ_c_1 = λ.^(c+1)
	λ_c_2 = λ.^(c+2)

	deriv_log = c ./ λ.^2
	#=print("deriv_log=", ntimes / 2 * deriv_log, "\n")=#
	deriv_trc = (- c*(c+1) / ϕ) .* r_ii ./  λ_c_2
	#=print("deriv_trc=", ntimes / 2 * deriv_trc, "\n")=#
	deriv_var =  (1/ϕ) .* sum(
	                          2*c*dist_from_mean ./ λ_c_1
	                       .- c*(c+1)*dist_from_mean.^2 ./ λ_c_2
	                       .- 1 ./ λ_c, 2)
	#=print("deriv_var=", deriv_var, "\n")=#
	deriv_λ = (ntimes/2) .* (deriv_log .+ deriv_trc) .+ deriv_var
	return deriv_λ
end

function Q_ϕ_second(
		λ::Vector{Float64},
		ϕ::Float64,
		c::Int,
		ntimes::Int,
		nedges::Int,
		tr::Float64,
		dist_from_mean::Array{Float64,2},
		)
	deriv_ϕ_log = +ntimes*nedges/(2*ϕ^2) 
	#=print("deriv_ϕ_log=", deriv_ϕ_log, "\n")=#
	deriv_ϕ_tr = -ntimes / 2 * tr / ϕ^2
	#=print("deriv_ϕ_tr=", deriv_ϕ_tr, "\n")=#
	deriv_ϕ_var = (-ϕ^-3)*sum(dist_from_mean.^2 ./ λ.^c)
	#=print("deriv_ϕ_var=", deriv_ϕ_var, "\n")=#
	deriv_ϕ = deriv_ϕ_log + deriv_ϕ_tr + deriv_ϕ_var
	return deriv_ϕ
end

function Q_ϕ_gradient(
		λ::Vector{Float64},
		ϕ::Float64,
		c::Int,
		ntimes::Int,
		nedges::Int,
		tr::Float64,
		dist_from_mean::Array{Float64,2},
		)
	# next up, ϕ
	deriv_ϕ_log = -ntimes*nedges/(2*ϕ) 
	#=print("deriv_ϕ_log=", deriv_ϕ_log, "\n")=#
	deriv_ϕ_tr = ntimes / 2 * tr / ϕ 
	#=print("deriv_ϕ_tr=", deriv_ϕ_tr, "\n")=#
	deriv_ϕ_var = (1/2/ϕ^2)*sum(dist_from_mean.^2 ./ λ.^c)
	#=print("deriv_ϕ_var=", deriv_ϕ_var, "\n")=#
	deriv_ϕ = deriv_ϕ_log + deriv_ϕ_tr + deriv_ϕ_var
	return deriv_ϕ
end

function Q_δλδϕ(
		λ::Vector{Float64},
		ϕ::Float64,
		c::Int,
		r_ii::Vector{Float64},
		ntimes::Int,
		nedges::Int,
		tr::Float64,
		dist_from_mean::Array{Float64,2},
		)
	deriv_trc = (+c / ϕ^2) .* r_ii .* λ.^(-(c+1))
	#=print("deriv_trc=", -ntimes / 2 * deriv_trc, "\n")=#
	#=deriv_var =  1/ϕ * sum(c .* (dist_from_mean.^2) ./ λ.^(c+1) - (dist_from_mean ./ λ.^c), 2)=#
	deriv_var =  (-1/ϕ^2) .* sum((c/2) .* (dist_from_mean.^2) ./ λ.^(c+1) .- (dist_from_mean ./ λ.^c), 2)
	#=print("deriv_var=", deriv_var, "\n")=#
	deriv_λ = (-ntimes/2) .* deriv_trc .+ deriv_var
	return deriv_λ
end

function eval_Q_gradient(params::XParams,
		constr::SigmaConstraints,
		conditional_var::Array{Float64,2}, 
		conditional_means::Array{Float64,2},
		)
	nedges, ntimes = size(conditional_means)
	c = constr.c
	ϕ = constr.ϕ
	r_ii = diag(conditional_var)
	tr = sum(r_ii./ diag(params.Σ))
	λ = params.λ
	sum_λ_c = sum(λ.^c)

	dist_from_mean = λ .- conditional_means
	# Q has two parts: the bit involving means, where we sum over the data
	# and the bit on the left involving sigma only
	deriv_λ = Q_λ_gradient(λ, ϕ, c, r_ii, ntimes, nedges, tr, dist_from_mean)
	deriv_ϕ = Q_ϕ_gradient(λ, ϕ, c,       ntimes, nedges, tr, dist_from_mean)

	return [deriv_λ; deriv_ϕ]
end

function eval_Q_hessian(params::XParams,
		constr::SigmaConstraints,
		conditional_var::Array{Float64,2}, 
		conditional_means::Array{Float64,2},
		)
	nedges, ntimes = size(conditional_means)
	c = constr.c
	ϕ = constr.ϕ
	r_ii = diag(conditional_var)
	tr = sum(r_ii./ diag(params.Σ))
	λ = params.λ
	sum_λ_c = sum(λ.^c)
	dist_from_mean = λ .- conditional_means

	hessian = Array(Float64, nedges+1, nedges+1)
	diag(hessian)[1:nedges] = Q_λ_second(
		λ,
		ϕ,
		c,
		r_ii,
		ntimes,
		nedges,
		tr,
		dist_from_mean,
		)
	δλδϕ = Q_δλδϕ(
		λ,
		ϕ,
		c,
		r_ii,
		ntimes,
		nedges,
		tr,
		dist_from_mean,
		)
	hessian[end,1:nedges] = δλδϕ
	hessian[1:nedges,end] = δλδϕ
	hessian[end,end] = Q_ϕ_second(
		λ,
		ϕ,
		c,
		ntimes,
		nedges,
		tr,
		dist_from_mean,
		)
	return hessian
end


function e_step(
		c::Int,
		y_arr::Array{Float64,2},
		current_θ::OptimParams,
		A::Array{Int,2},
		)
	constr = SigmaConstraints(c, current_θ.ϕ)
	params = XParams(current_θ.λ, constr)

	projection_mat = projection_matrix(params.Σ, A)
	R  = conditional_variance(params, projection_mat, A)
	conditional_means = iterate_conditional_means(y_arr, params, constr, projection_mat, A)
	return (conditional_means, R)
end

function m_step(
		c::Int,
		conditional_means::Array{Float64,2},
		conditional_var::Array{Float64,2},
		prev_θ::OptimParams;
		test_gradient=false,
		)

	function m_step_eval(x::Vector)
		λ = exp(x[1:end-1])
		ϕ = exp(x[end])
		constr = SigmaConstraints(c, ϕ)
		params = XParams(λ, constr)
		Q_ = try 
			eval_Q(params, conditional_var, conditional_means)
		catch
			-1000000.0
		end
		#=print("Q=", Q_, "\n")=#
		return -Q_
	end
	
	function m_step_derv!(x::Vector, storage::Vector)
		λ = exp(x[1:end-1])
		ϕ = exp(x[end])
		constr = SigmaConstraints(c, ϕ)
		params = XParams(λ, constr)
		δQδθ = eval_Q_gradient(params, constr, conditional_var, conditional_means)
		#=print("gradient=", -δQδθ .* [λ; ϕ])=#
		storage[:] = -δQδθ .* [λ; ϕ]
	end
	
	function m_step_hess!(x::Vector, storage::Array{Float64,2})
		print("\n######HESSIAN!####\n")
		λ = exp(x[1:end-1])
		ϕ = exp(x[end])
		constr = SigmaConstraints(c, ϕ)
		params = XParams(λ, constr)
		δ2Qδθ2 = eval_Q_hessian(params, constr, conditional_var, conditional_means)
		jacobian = [λ; ϕ] * transpose([λ; ϕ])
		storage[:,:] = -δ2Qδθ2 .* jacobian
	end
	
	d3 = TwiceDifferentiableFunction(
			m_step_eval,
			m_step_derv!,
			m_step_hess!,
			)
	
	nedges = length(prev_θ.λ)
	start = [prev_θ.λ;prev_θ.ϕ]
	
	if test_gradient
		print("start: ", start, "\n")
		print("log(start): ", log(start), "\n")
		Q = m_step_eval(log(start))
		print("Q: ", Q)
		hessian = Array(Float64, nedges+1, nedges+1)
		gradient = copy(start)
		m_step_derv!(log(start), gradient)
		m_step_hess!(log(start), hessian)
		delta = 0.01
		delta1 = 0.1
		for ipar in 1:length(start)
			modified = log(copy(start))
			modified[ipar] += delta
			print("modified: ", modified, "\n")

			expected = Q + delta * gradient[ipar]
			actual = m_step_eval(modified)

			expected_grad = gradient .+ (delta .* hessian[:,ipar])
			actual_gradient = copy(start)
			m_step_derv!(log(start), actual_gradient)

			@printf("Q: %.3f; exp_diff: %.3f; act_diff: %.3f; expected: %.3f, actual: %.3f\n", Q, delta*gradient[ipar], actual-Q, expected, actual)
			@assert expected - delta/10 * abs(gradient[ipar]) < actual < expected + delta/10 * abs(gradient[ipar])
			print("gradient: ", gradient, "\n")
			print("expected gradient: ", expected_grad, "\n")
			print("actual gradient", actual_gradient, "\n")
			print("hessian slice", hessian[:,ipar], "\n")
			@assert all(expected_grad .- delta/10 * abs(hessian[:,ipar]) .< actual_gradient .< expected .+ delta/10 * abs(hessian[:,ipar]))
			@printf("Parameter %d ok\n", ipar)
		end
	end
	
	m_result = Optim.optimize(
		d3, 
		log(start), 
		method=:l_bfgs,
		#=ftol=0.1,=#
		)
	#=print("-Q=", m_result.f_minimum, "\n")=#
	θ = exp(m_result.minimum)
	new_θ = OptimParams(θ[1:nedges], θ[end])
	return (m_result.f_minimum, new_θ)
end

function em_algo(
		c::Int,
		y_arr::Array{Float64,2},
		init_θ::OptimParams,
		A::Array{Int,2},
		)
	current_θ = init_θ
	previous_Q::Float64 = Inf
	for istep = 1:10000
		conditional_means, conditional_var = e_step(
				c,
				y_arr,
				current_θ,
				A
				)
		current_Q, current_θ = m_step(
				c,
				conditional_means,
				conditional_var,
				current_θ,
				test_gradient=false,
				)
		if abs(previous_Q-current_Q)/current_Q < 1e-4
			#=@printf("breaking after %d steps", istep)=#
			break
		end
		previous_Q = current_Q
	end
	return current_θ
end

immutable WindowedY
	y_arr::Array{Float64,2}
	window_width::Uint32
end
function Base.start(w::WindowedY)
	return w.window_width+1
end
function Base.next(w::WindowedY, state::Int)
	return w.y_arr[:,state-w.window_width:state+w.window_width], state+1
end
function Base.done(w::WindowedY, state::Int)
	return state+w.window_width > size(w.y_arr)[2]
end

function build_V(local_fit::Array{Float64,2})
	var_iid_fit = var(log(local_fit),2)
	return Diagonal(vec(var_iid_fit))
end

function smoothing_cost(η::Vector{Float64}, # η is logθ
                    prev_η::Vector{Float64},
                    V::AbstractArray{Float64,2},
                   )
	move = η - prev_η 
	cost = transpose(move) * inv(V) * move
	return cost[1]
end

function smoothing_grad(η::Vector{Float64}, # η is logθ
                    prev_η::Vector{Float64},
                    V::AbstractArray{Float64,2},
                   )
	move = η - prev_η 
	grad = 2 * transpose(move) * inv(V)
	return vec(grad)
end


function smooth_m_step(
		c::Int,
		conditional_means::Array{Float64,2},
		conditional_var::Array{Float64,2},
		prev_θ::OptimParams,
		V::AbstractArray{Float64,2};
		test_gradient=false,
		)
	prev_logθ = log([prev_θ.λ; prev_θ.ϕ])

	function m_step_eval(x::Vector)
		λ = exp(x[1:end-1])
		ϕ = exp(x[end])
		constr = SigmaConstraints(c, ϕ)
		params = XParams(λ, constr)
		Q_ = try 
			eval_Q(params, conditional_var, conditional_means)
		catch
			-1000000
		end
		#=print("Q=", Q_, "\n")=#
		return -Q_ + smoothing_cost(x, prev_logθ, V)
	end
	
	function m_step_derv!(x::Vector, storage::Vector)
		λ = exp(x[1:end-1])
		ϕ = exp(x[end])
		constr = SigmaConstraints(c, ϕ)
		params = XParams(λ, constr)
		δQδθ = eval_Q_gradient(params, constr, conditional_var, conditional_means)
		storage[:] = -δQδθ .* [λ; ϕ] .+ smoothing_grad(x, prev_logθ, V)

	end
	
	d2 = DifferentiableFunction(
			m_step_eval,
			m_step_derv!,
			#=m_step_hess!,=#
			)
	
	nedges = length(prev_θ.λ)
	start = [prev_θ.λ;prev_θ.ϕ]
	
	if test_gradient
		print("start: ", start, "\n")
		print("log(start): ", log(start), "\n")
		Q = m_step_eval(log(start))
		print("Q: ", Q)
		hessian = Array(Float64, nedges+1, nedges+1)
		gradient = copy(start)
		m_step_derv!(log(start), gradient)
		m_step_hess!(log(start), hessian)
		delta = 0.01
		delta1 = 0.1
		for ipar in 1:length(start)
			modified = log(copy(start))
			modified[ipar] += delta
			print("modified: ", modified, "\n")

			expected = Q + delta * gradient[ipar]
			actual = m_step_eval(modified)

			expected_grad = gradient .+ (delta .* hessian[:,ipar])
			actual_gradient = copy(start)
			m_step_derv!(log(start), actual_gradient)

			@printf("Q: %.3f; exp_diff: %.3f; act_diff: %.3f; expected: %.3f, actual: %.3f\n", Q, delta*gradient[ipar], actual-Q, expected, actual)
			@assert expected - delta/10 * abs(gradient[ipar]) < actual < expected + delta/10 * abs(gradient[ipar])
			print("gradient: ", gradient, "\n")
			print("expected gradient: ", expected_grad, "\n")
			print("actual gradient", actual_gradient, "\n")
			print("hessian slice", hessian[:,ipar], "\n")
			@assert all(expected_grad .- delta/10 * abs(hessian[:,ipar]) .< actual_gradient .< expected .+ delta/10 * abs(hessian[:,ipar]))
			@printf("Parameter %d ok\n", ipar)
		end
	end
	
	m_result = Optim.optimize(
		d2, 
		log(start), 
		method=:l_bfgs,
		#=ftol=0.1,=#
		)
	#=print("-Q=", m_result.f_minimum, "\n")=#
	θ = exp(m_result.minimum)
	new_θ = OptimParams(θ[1:nedges], θ[end])
	return (m_result.f_minimum, new_θ)
end

function smooth_em_algo(
		c::Int,
		y_arr::Array{Float64,2},
		init_θ::OptimParams,
		A::Array{Int,2},
		V::AbstractArray{Float64,2},
		)
	current_θ = init_θ
	previous_Q::Float64 = Inf
	for istep = 1:10000
		conditional_means, conditional_var = e_step(
				c,
				y_arr,
				current_θ,
				A
				)
		current_Q, current_θ = smooth_m_step(
				c,
				conditional_means,
				conditional_var,
				current_θ,
				V,
				test_gradient=false,
				)
		if abs(previous_Q-current_Q)/current_Q < 1e-4
			#=@printf("breaking after %d steps", istep)=#
			break
		end
		previous_Q = current_Q
	end
	return current_θ
end

function figure5(
		times::AbstractArray{Any,1},
		edge_table::Array{ASCIIString,2},
		edges::Vector{ASCIIString},
		src::Vector{ASCIIString},
		dst::Vector{ASCIIString},
		transfer::Array{Int,2},
		y_arr::Array{Float64,2},
		x_arr::Array{Float64,2},
		local_fit::Array{Float64,2},
		window_size::Int,
	)
	times_any = sort(unique(times))
	times_arr = Float64[hour(t) + minute(t)/60.0 + second(t)/3600.0 for t in times_any]
	ntimes = length(times_any)
	nedges = length(edges)
	nnodes = size(edge_table)[1]
	window_width = (window_size-1)/2
	
	rotated = copy(edge_table)
	for irow in 1:nnodes
		#=rotated[irow,:] = rotated[irow,end:-1:1]=#
		rotated[:,irow] = rotated[end:-1:1,irow]
	end
	
	y_lambda = transfer * local_fit[1:end-1,:]
	
	boxsmooth = Array(Float64, nedges, ntimes-window_size+1)
	for (t, local_x) in enumerate(WindowedY(x_arr, 5))
		boxsmooth[:,t] = mean(local_x,2)
	end
	
	y_smooth = Array(Float64, nnodes*2, ntimes-window_size+1)
	for (t, local_y) in enumerate(WindowedY(y_arr, 5))
		y_smooth[:,t] = mean(local_y,2)
	end
	#=y_smooth = transfer * boxsmooth=#
		
	plt.close("all")
	for (idst, destination) in enumerate(dst)
		plotnum = (nnodes+1)-idst
		subplot(nnodes+1,nnodes+1, plotnum)
		plot(times_arr[window_width+1:end-window_width], 
			transpose(y_lambda[idst+nnodes,:])[:,1],
			linewidth=2,
			color="cyan",
			)
		plot(times_arr[window_width+1:end-window_width], 
			transpose(y_smooth[idst+nnodes,:])[:,1],
			linewidth=1,
			color="black",
			)
		xlim(0,24)
		ylim(0,1e6)
		if plotnum % (nnodes+1) == 1
			yticks((0,2e5,4e5,6e5,8e5,10e5), ("0", "200K", "400K", "600K", "800K", "1M"), fontsize=9)
		else
			yticks(())
		end
		if plotnum <= nnodes * (nnodes+1)
			xticks(())
		else
			xticks((0,4,8,12,16,20,24), fontsize=9)
		end
	
		title(destination, fontsize=9)
	end
	
	plotnum = nnodes+1
	subplot(nnodes+1,nnodes+1, plotnum)
	plot(times_arr[window_width+1:end-window_width], 
		transpose(sum(local_fit[1:end-1,:],1))[:,1],
		linewidth=2,
		color="cyan",
		)
	plot(times_arr[window_width+1:end-window_width], 
		transpose(sum(y_smooth,1)./2)[:,1],
		linewidth=1,
		color="black",
		)
	xlim(0,24)
	ylim(0,1e6)
	if plotnum % nnodes+1 == 1
		yticks((0,2e5,4e5,6e5,8e5,10e5), ("0", "200K", "400K", "600K", "800K", "1M"), fontsize=9)
	else
		yticks(())
	end
	if plotnum <= nnodes * (nnodes+1)
		xticks((), fontsize=9)
	else
		xticks((0,4,8,12,16,20,24), fontsize=9)
	end
	title("total", fontsize=9)
		
	for (isrc, source) in enumerate(src)
		plotnum = nnodes+1 + isrc*(nnodes+1)
		subplot(nnodes+1,nnodes+1, plotnum)
		plot(times_arr[window_width+1:end-window_width], 
			transpose(y_lambda[isrc,:])[:,1],
			linewidth=2,
			color="cyan",
			)
		plot(times_arr[window_width+1:end-window_width], 
			transpose(y_smooth[isrc,:])[:,1],
			linewidth=1,
			color="black",
			)
		xlim(0,24)
		ylim(0,1e6)
		if plotnum % nnodes+1 == 1
			yticks((0,2e5,4e5,6e5,8e5,10e5), ("0", "200K", "400K", "600K", "800K", "1M"), fontsize=9)
		else
			yticks(())
		end
		if plotnum <= nnodes * (nnodes+1)
			xticks(())
		else
			xticks((0,4,8,12,16,20,24), fontsize=9)
		end
		title(source, fontsize=9)
	end
	
	for (itedge, edge) in enumerate(rotated)
		print(edge)
		iedge = findfirst(edges, edge)
		plotnum = nnodes+1 + itedge + div(itedge-1,nnodes)
		subplot(nnodes+1,nnodes+1, plotnum)
		plot(times_arr[window_width+1:end-window_width], 
			transpose(local_fit[iedge,:])[:,1],
			linewidth=2,
			color="cyan",
			)
		plot(times_arr[window_width+1:end-window_width], 
			transpose(boxsmooth[iedge,:])[:,1],
			linewidth=1,
			color="black",
			)
		xlim(0,24)
		ylim(0,1e6)
		if plotnum % (nnodes+1) == 1
			yticks((0,2e5,4e5,6e5,8e5,10e5), ("0", "200K", "400K", "600K", "800K", "1M"), fontsize=9)
		else
			yticks(())
		end
		if plotnum <= nnodes * (nnodes+1)
			xticks(())
		else
			xticks((0,4,8,12,16,20,24), fontsize=9)
		end
		if plotnum == (nnodes) * (nnodes+1) + div(nnodes,2)
			xlabel("hour of day", fontsize=15)
		end
		if plotnum == (nnodes+1) * div(nnodes,2) + 1
			ylabel("bytes/sec", fontsize=15)
		end
		title(edge, fontsize=9)
	
		#=fig[:autofmt_xdate](bottom=0.2,rotation=30,ha="right") # Adjust labeling=#
		#=fig[:canvas][:draw]() # Update the figure=#
	end
	fig=gcf()
	fig[:set_size_inches](15.0,15.0)
	subplots_adjust(wspace=0, hspace=0.25, right=0.95, top=0.95, left=0.1)
	savefig("mrischard_fig5.pdf")
end
		
function figure6(
		times::AbstractArray{Any,1},
		edge_table::Array{ASCIIString,2},
		edges::Vector{ASCIIString},
		src::Vector{ASCIIString},
		dst::Vector{ASCIIString},
		transfer::Array{Int,2},
		y_arr::Array{Float64,2},
		x_arr::Array{Float64,2},
		local_fit::Array{Float64,2},
		smooth_fit::Array{Float64,2},
		window_size::Int,
	)
	times_any = sort(unique(times))
	times_arr = Float64[hour(t) + minute(t)/60.0 + second(t)/3600.0 for t in times_any]
	ntimes = length(times_any)
	nedges = length(edges)
	nnodes = size(edge_table)[1]
	window_width = (window_size-1)/2
	
	rotated = copy(edge_table)
	for irow in 1:nnodes
		#=rotated[irow,:] = rotated[irow,end:-1:1]=#
		rotated[:,irow] = rotated[end:-1:1,irow]
	end

	boxsmooth = Array(Float64, nedges, ntimes-window_size+1)
	for (t, local_x) in enumerate(WindowedY(x_arr, 5))
		boxsmooth[:,t] = mean(local_x,2)
	end
	
	#=y_smooth = transfer * boxsmooth=#
	y_smooth = Array(Float64, nnodes*2, ntimes-window_size+1)
	for (t, local_y) in enumerate(WindowedY(y_arr, 5))
		y_smooth[:,t] = mean(local_y,2)
	end
	
	y_lambda = transfer * local_fit[1:end-1,:]
	y_prior = transfer * smooth_fit[1:end-1,:]
		
	plt.close("all")
	for (idst, destination) in enumerate(dst)
		plotnum = nnodes+1-idst
		subplot(nnodes+1,nnodes+1, plotnum)
		plot(times_arr[window_width+1:end-window_width], 
			transpose(y_lambda[idst+4,:])[:,1],
			linewidth=2,
			color="cyan",
			)
		plot(times_arr[window_width+1:end-window_width], 
			transpose(y_prior[idst+4,:])[:,1],
			linewidth=2,
			color="magenta",
			)
		plot(times_arr[window_width+1:end-window_width], 
			transpose(y_smooth[idst+4,:])[:,1],
			linewidth=1,
			color="black",
			)
		xlim(0,24)
		ylim(0,5e4)
		if plotnum % (nnodes+1) == 1
			yticks((0,2e5,4e5,6e5,8e5,10e5), ("0","10K","20K","30K","40K","50K"), fontsize=9)
		else
			yticks(())
		end
		if plotnum <= nnodes * (nnodes+1)
			xticks(())
		else
			xticks((0,4,8,12,16,20,24), fontsize=9)
		end
	
		title(destination, fontsize=9)
	end
		
	for (isrc, source) in enumerate(src)
		plotnum = (nnodes+1) + isrc*(nnodes+1)
		subplot((nnodes+1),(nnodes+1), plotnum)
		plot(times_arr[window_width+1:end-window_width], 
			transpose(y_lambda[isrc,:])[:,1],
			linewidth=2,
			color="cyan",
			)
		plot(times_arr[window_width+1:end-window_width], 
			transpose(y_prior[isrc,:])[:,1],
			linewidth=2,
			color="magenta",
			)
		plot(times_arr[window_width+1:end-window_width], 
			transpose(y_smooth[isrc,:])[:,1],
			linewidth=1,
			color="black",
			)
		xlim(0,24)
		ylim(0,5e4)
		if plotnum % (nnodes+1) == 1
			yticks((0,2e5,4e5,6e5,8e5,10e5), ("0", "200K", "400K", "600K", "800K", "1M"), fontsize=9)
		else
			yticks(())
		end
		if plotnum <= nnodes * (nnodes+1)
			xticks(())
		else
			xticks((0,4,8,12,16,20,24), fontsize=9)
		end
		title(source, fontsize=9)
	end

	plotnum = nnodes+1
	subplot(nnodes+1,nnodes+1, plotnum)
	plot(times_arr[window_width+1:end-window_width], 
		transpose(sum(local_fit[1:end-1,:],1))[:,1],
		linewidth=2,
		color="cyan",
		)
	plot(times_arr[window_width+1:end-window_width], 
		transpose(sum(y_prior[1:end-1,:],1)./2)[:,1],
		linewidth=2,
		color="magenta",
		)
	plot(times_arr[window_width+1:end-window_width], 
		transpose(sum(y_smooth,1)./2)[:,1],
		linewidth=1,
		color="black",
		)
	xlim(0,24)
	ylim(0,5e4)
	if plotnum % nnodes+1 == 1
		yticks((0,2e5,4e5,6e5,8e5,10e5), ("0", "200K", "400K", "600K", "800K", "1M"), fontsize=9)
	else
		yticks(())
	end
	if plotnum <= nnodes * (nnodes+1)
		xticks((), fontsize=9)
	else
		xticks((0,4,8,12,16,20,24), fontsize=9)
	end
	title("total", fontsize=9)
	
	for (itedge, edge) in enumerate(rotated)
		print(edge)
		iedge = findfirst(edges, edge)
		plotnum = (nnodes+1) + itedge + div(itedge-1,nnodes)
		subplot((nnodes+1),(nnodes+1), plotnum)
		plot(times_arr[window_width+1:end-window_width], 
			transpose(local_fit[iedge,:])[:,1],
			linewidth=2,
			color="cyan",
			)
		plot(times_arr[window_width+1:end-window_width], 
			transpose(smooth_fit[iedge,:])[:,1],
			linewidth=2,
			color="magenta",
			)
		plot(times_arr[window_width+1:end-window_width], 
			transpose(boxsmooth[iedge,:])[:,1],
			linewidth=1,
			color="black",
			)
		xlim(0,24)
		ylim(0,5e4)
		if plotnum % (nnodes+1) == 1
			yticks((0,1e4,2e4,3e4,4e4,5e4),("0","10K","20K","30K","40K","50K"), fontsize=9)
		else
			yticks(())
		end
		if plotnum <= nnodes * (nnodes+1)
			xticks(())
		else
			xticks((0,4,8,12,16,20,24), fontsize=9)
		end
		if plotnum == (nnodes) * (nnodes+1) + div(nnodes,2)
			xlabel("hour of day", fontsize=15)
		end
		if plotnum == (nnodes+1) * div(nnodes,2) + 1
			ylabel("bytes/sec", fontsize=15)
		end
		title(edge, fontsize=9)
	
	end
	fig=gcf()
	fig[:set_size_inches](15.0,15.0)
	subplots_adjust(wspace=0, hspace=0.25, right=0.95, top=0.95, left=0.1)
	savefig("mrischard_fig6.pdf")
end

function analyse(router_df::DataFrame,
				 nodes::Vector{ASCIIString};
				 src_prefix="src", 
				 dst_prefix="dst",
				 have_x=true,
				)
	src = ASCIIString[src_prefix*" "*n for n in nodes]
	dst = ASCIIString[dst_prefix*" "*n for n in nodes]

	str_edge_table = ASCIIString[src*"->"*dst for dst in nodes, src in nodes]
	str_edges = vec(str_edge_table)

	tup_edge_table = (ASCIIString,ASCIIString)[(src, dst) for dst in nodes, src in nodes]
	tup_edges = vec(tup_edge_table)
	nedges = length(str_edges)

	grouped_router = groupby(router_df, :time)
	ntimes = length(grouped_router)
	columns = convert(Array{String,1}, sort(unique(router_df[:nme])))
	grouped_values = [g[:value] for g in grouped_router]
	values_arr = Array(Float64, length(columns), ntimes)
	for t = 1:ntimes
		values_arr[:,t] = grouped_router[t][:value]
	end
	y_arr = extract_yt(values_arr, columns, nodes, src_prefix=src_prefix, dst_prefix=dst_prefix)
	
	transfer = transfer_matrix(nodes, tup_edges)
	if have_x
		x_arr = extract_xt(values_arr, columns, str_edges)
		for t = 1:ntimes
			@assert sum(abs(y_arr[:,t]-transfer*x_arr[:,t])) < 1
		end
	else
		x_arr = zeros(Float64, nedges, ntimes)
	end
	A = transfer[1:size(transfer)[1]-1,:]
	init_ϕ = var(router_df[:value]) / mean(router_df[:value])
	init_ϕ = 1
	init_constr = SigmaConstraints(2, init_ϕ)  # c,ϕ
	init_λ=mean(y_arr) / 4 * ones(Float64, length(str_edges))
	init_params = XParams(init_λ, init_constr)
	init_θ = OptimParams(init_λ, init_ϕ)
	print("Initial Likelihood", likelihood(init_θ, y_arr, A, init_constr.c))
	
	c = init_constr.c
	
	#=print("c", c, "y_arr", y_arr, "init_θ", init_θ, "A", A)=#
	mle_θ = em_algo(c, y_arr, init_θ, A)
	print(mle_θ)
	print("Final Likelihood", likelihood(mle_θ, y_arr, A, init_constr.c))
	
	prev_θ = mle_θ
	window_size = 11
	window_width = (window_size-1)/2
	local_fit = Array(Float64, nedges+1, ntimes-window_size+1)
	for (t, local_y) in enumerate(WindowedY(y_arr, 5))
		print(t, ":")
		θ = try 
			em_algo(c, local_y, prev_θ, A)
		catch
			em_algo(c, local_y, mle_θ, A)
		end
		local_fit[:,t] = [θ.λ; θ.ϕ]
		prev_θ = θ
	end
	
	
	figure5(
			router_df[:time],
			str_edge_table,
			str_edges,
			src,
			dst,
			transfer,
			y_arr,
			x_arr,
			local_fit,
			window_size,
		)
	
	
	print("SMOOTH")
	V = build_V(local_fit)
	mean_θ = vec(mean(local_fit,2))
	prev_θ = OptimParams(mean_θ[1:end-1], mean_θ[end])
	smooth_fit = Array(Float64, nedges+1, ntimes-window_size+1)
	for (t, local_y) in enumerate(WindowedY(y_arr, 5))
		print(t, ":")
		θ = smooth_em_algo(c, local_y, prev_θ, A, V)
		smooth_fit[:,t] = [θ.λ; θ.ϕ]
		prev_θ = θ
	end
	
	figure6(
			router_df[:time],
			str_edge_table,
			str_edges,
			src,
			dst,
			transfer,
			y_arr,
			x_arr,
			local_fit,
			smooth_fit,
			window_size,
		)
end


