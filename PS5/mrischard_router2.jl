include("mrischard_plot_functions.jl")
include("mrischard_julia.jl")
include("mrischard_smoothed.jl")

router2 = read_router("2router_linkcount.dat")
router_df = router2
