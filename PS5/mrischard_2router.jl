include("mrischard_functions.jl")

router2_ = read_router("2router_linkcount.dat")
nodes_2 = ASCIIString[
 "gw-others",
 "gw3",
 "gw2",
 "gw1",
 "r4-others",
 "switch",
 "r4-local",
 "router5",
]
analyse(router2_, nodes_2, src_prefix="ori", have_x=false)
