library(RUnit)
source("solving.r")

is_near <- function(x, x0, tol=1e-9) {
	return(abs(x-x0) < tol)
}

test.density.integral <- function(){
	increment <- 0.005
	grid <- seq(0,1,by=increment)
	#l <- length(grid)
	#grid <- grid[3:l-1]
	get_simplex <- function(x){
		y <- seq(0, 1-x, by=increment)
		xx <- rep(x, length(y))
		xy <- rbind(xx, y)
		return(xy)
	}
	simplex <- sapply(grid, get_simplex)
	flat_simplex <- do.call(cbind, simplex)
	# flat_simplex is a grid over to simplex
	mu = c(1,1)
	alpha=3
	beta=0.1
	get_density <- function(u) {
		return(dlogisticnorm(u, mu, alpha, beta))
	}
	density <- apply(flat_simplex, 2, get_density)

	integral <- sum(density)*increment^2
	checkTrue(is_near(integral, 1.0, tol=increment*10))
}

run.tests <- function() {
	test.suite <- defineTestSuite(name="density", dirs=".", testFileRegexp="tests.r")
	test.result <- runTestSuite(test.suite)
	printTextProtocol(test.result)
}
