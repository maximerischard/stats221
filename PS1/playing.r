library(mvtnorm)
mean <- c(0.8, 0.5)
alpha <- 1.0
beta <- 0.4
sigma <- alpha*diag(2)
sigma[upper.tri(sigma)] <- -beta
sigma[lower.tri(sigma)] <- -beta

x=seq(-3,3,length.out=100)
y=seq(-3,3,length.out=100)
xy <- expand.grid(x=seq(-3,3,length.out=100), y=seq(-3,3,length.out=100))
z <- dmvnorm(x=xy, mean=mean, sigma=sigma)
zmat <- matrix(z, 100, 100)
contour(x, y, zmat)

alpha <- 3
beta <- 7
d <- 10
sigma_cov <- diag(d)*(alpha+beta) - beta
1/(alpha+beta)*(diag(d) - (beta)/(alpha+beta)/(1-(beta*d)*1.0/(alpha+beta)) * (one %*% t(one)))

# should match
det(sigma_cov)
(alpha+beta)^d - beta*d*(alpha+beta)^(d-1)

# should match
sigma_inv <- solve(sigma_cov)
1/(alpha+beta)*(diag(d) + (beta)/(alpha+beta)/(1-(beta*d)*1.0/(alpha+beta)) * (one %*% t(one)))
1/(alpha+beta)*(diag(d) + (beta)/(alpha+beta-d*beta) * (one %*% t(one)))

alpha <- 8.4
beta <- 1.2
d <- 4
sigma_cov <- diag(d)*(alpha+beta) - beta
eigen(sigma_cov)$values
alpha+beta-d*beta
